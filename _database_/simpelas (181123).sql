-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 23, 2018 at 11:14 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `simpelas`
--
CREATE DATABASE IF NOT EXISTS `simpelas` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `simpelas`;

-- --------------------------------------------------------

--
-- Table structure for table `admin_login`
--

CREATE TABLE IF NOT EXISTS `admin_login` (
  `admin_username` varchar(20) NOT NULL,
  `admin_password` text NOT NULL,
  PRIMARY KEY (`admin_username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_login`
--

INSERT INTO `admin_login` (`admin_username`, `admin_password`) VALUES
('admin', '1');

-- --------------------------------------------------------

--
-- Table structure for table `ambil_matakuliah`
--

CREATE TABLE IF NOT EXISTS `ambil_matakuliah` (
  `id_ambil` int(3) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `id_matkul` int(3) unsigned zerofill NOT NULL,
  `mahasiswa_username` varchar(12) NOT NULL,
  PRIMARY KEY (`id_ambil`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `ambil_matakuliah`
--

INSERT INTO `ambil_matakuliah` (`id_ambil`, `id_matkul`, `mahasiswa_username`) VALUES
(006, 002, '160533611471'),
(007, 001, '160533611580'),
(008, 001, '160533611470'),
(015, 003, '160533611580');

-- --------------------------------------------------------

--
-- Table structure for table `dosen_login`
--

CREATE TABLE IF NOT EXISTS `dosen_login` (
  `dosen_username` varchar(18) NOT NULL,
  `dosen_password` text NOT NULL,
  `dosen_nama` varchar(60) NOT NULL,
  `dosen_email` varchar(70) NOT NULL,
  `dosen_notelp` varchar(16) NOT NULL DEFAULT '+62800000000',
  PRIMARY KEY (`dosen_username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dosen_login`
--

INSERT INTO `dosen_login` (`dosen_username`, `dosen_password`, `dosen_nama`, `dosen_email`, `dosen_notelp`) VALUES
('198909112018031002', '1', 'Wahyu Nur', 'wahyu.nur.ft@um.ac.id', '+62800000000'),
('198909112018031009', '1', 'Dosen 1', 'dosen@um.ac.id', '+62800000000');

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa_login`
--

CREATE TABLE IF NOT EXISTS `mahasiswa_login` (
  `mahasiswa_username` varchar(12) NOT NULL,
  `mahasiswa_password` text NOT NULL,
  `mahasiswa_nama` varchar(60) NOT NULL,
  `mahasiswa_email` varchar(70) NOT NULL,
  `mahasiswa_notelp` varchar(16) NOT NULL DEFAULT '+62800000000',
  PRIMARY KEY (`mahasiswa_username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mahasiswa_login`
--

INSERT INTO `mahasiswa_login` (`mahasiswa_username`, `mahasiswa_password`, `mahasiswa_nama`, `mahasiswa_email`, `mahasiswa_notelp`) VALUES
('160533611470', '1', 'Rendy Yofana', 'rendyyofana@email.com', '+6281222222222'),
('160533611471', '1', 'Rendy A', 'rendy@email.com', '+6281333333333'),
('160533611580', '1', 'Rendy Yofan', 'rendyy@email.com', '+628123399999');

-- --------------------------------------------------------

--
-- Table structure for table `matakuliah`
--

CREATE TABLE IF NOT EXISTS `matakuliah` (
  `id_matkul` int(3) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `dosen_username` varchar(18) NOT NULL,
  `nama_matkul` varchar(60) NOT NULL,
  PRIMARY KEY (`id_matkul`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `matakuliah`
--

INSERT INTO `matakuliah` (`id_matkul`, `dosen_username`, `nama_matkul`) VALUES
(001, '1989091120180310', 'Pemrograman Web - AA'),
(002, '1989091120180310', 'Pemrograman Web - B'),
(003, '1989091120180309', 'Cobaa');

-- --------------------------------------------------------

--
-- Table structure for table `tugas`
--

CREATE TABLE IF NOT EXISTS `tugas` (
  `id_tugas` int(3) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `id_matkul` int(3) unsigned zerofill NOT NULL,
  `nama_tugas` varchar(50) NOT NULL,
  `format_file` text NOT NULL,
  `format_nama_delimiter` varchar(1) NOT NULL,
  `format_nama` text,
  `deskripsi` text NOT NULL,
  `waktu_awal` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `waktu_akhir` datetime NOT NULL,
  PRIMARY KEY (`id_tugas`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tugas`
--

INSERT INTO `tugas` (`id_tugas`, `id_matkul`, `nama_tugas`, `format_file`, `format_nama_delimiter`, `format_nama`, `deskripsi`, `waktu_awal`, `waktu_akhir`) VALUES
(001, 001, 'Tugas Pemrograman Web - A (1)', 'jpg|jpeg|png|rar|zip|7z', '-', 'mahasiswa_nama-mahasiswa_username-nama_tugas', 'Test', '2018-11-19 18:07:41', '2018-11-30 06:00:00'),
(002, 002, 'Tugas Pemrograman Web - B (2)', 'jpg|jpeg|png|pdf', '-', 'mahasiswa_nama', 'Adalah', '2018-11-19 18:55:26', '2018-11-30 06:00:00'),
(003, 001, 'Modul 1 - HTML', 'pdf', '.', 'mahasiswa_nama.mahasiswa_username.nama_tugas', 'ini deskripsi', '2018-11-20 13:05:40', '2018-11-20 13:10:00');

-- --------------------------------------------------------

--
-- Table structure for table `tugas_kumpul`
--

CREATE TABLE IF NOT EXISTS `tugas_kumpul` (
  `id_kumpul` int(3) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `id_tugas` int(3) unsigned zerofill NOT NULL,
  `mahasiswa_username` varchar(12) NOT NULL,
  `nama_file` text NOT NULL,
  `nilai` int(3) NOT NULL DEFAULT '0',
  `waktu_upload` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `waktu_nilai` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_kumpul`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `tugas_kumpul`
--

INSERT INTO `tugas_kumpul` (`id_kumpul`, `id_tugas`, `mahasiswa_username`, `nama_file`, `nilai`, `waktu_upload`, `waktu_nilai`) VALUES
(005, 002, '160533611471', 'RendyA.png', 100, '2018-11-21 23:10:42', '2018-11-21 22:30:56'),
(006, 002, '160533611470', 'RendyYofana.png', 0, '2018-11-21 22:30:56', '2018-11-21 22:30:56'),
(008, 001, '160533611470', 'RendyYofana-160533611470-TugasPemrogramanWebA(1).jpeg', 1, '2018-11-22 08:49:17', '2018-11-22 08:49:17');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
