-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 12, 2018 at 02:41 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `simpelas`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_login`
--

CREATE TABLE IF NOT EXISTS `admin_login` (
  `admin_username` varchar(20) NOT NULL,
  `admin_password` text NOT NULL,
  PRIMARY KEY (`admin_username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_login`
--

INSERT INTO `admin_login` (`admin_username`, `admin_password`) VALUES
('admin', '1');

-- --------------------------------------------------------

--
-- Table structure for table `ambil_matakuliah`
--

CREATE TABLE IF NOT EXISTS `ambil_matakuliah` (
  `id_ambil` int(3) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `id_matkul` int(3) unsigned zerofill NOT NULL,
  `mahasiswa_username` varchar(12) NOT NULL,
  PRIMARY KEY (`id_ambil`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `ambil_matakuliah`
--

INSERT INTO `ambil_matakuliah` (`id_ambil`, `id_matkul`, `mahasiswa_username`) VALUES
(001, 001, '160533611400'),
(002, 001, '160533611470');

-- --------------------------------------------------------

--
-- Table structure for table `dosen_login`
--

CREATE TABLE IF NOT EXISTS `dosen_login` (
  `dosen_username` varchar(18) NOT NULL,
  `dosen_password` text NOT NULL,
  PRIMARY KEY (`dosen_username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dosen_login`
--

INSERT INTO `dosen_login` (`dosen_username`, `dosen_password`) VALUES
('1989091120180310', '1');

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa_login`
--

CREATE TABLE IF NOT EXISTS `mahasiswa_login` (
  `mahasiswa_username` varchar(12) NOT NULL,
  `mahasiswa_password` text NOT NULL,
  `mahasiswa_nama` varchar(60) NOT NULL,
  `mahasiswa_email` varchar(70) NOT NULL,
  `mahasiswa_notelp` varchar(16) NOT NULL DEFAULT '+62800000000',
  PRIMARY KEY (`mahasiswa_username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mahasiswa_login`
--

INSERT INTO `mahasiswa_login` (`mahasiswa_username`, `mahasiswa_password`, `mahasiswa_nama`, `mahasiswa_email`, `mahasiswa_notelp`) VALUES
('160533611470', '1', 'Rendy Yofana', 'rendyyofana@email.com', '+6281222222222'),
('160533611471', '1', 'Rendy A', 'rendy@email.com', '+6281333333333');

-- --------------------------------------------------------

--
-- Table structure for table `matakuliah`
--

CREATE TABLE IF NOT EXISTS `matakuliah` (
  `id_matkul` int(3) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `dosen_username` varchar(18) NOT NULL,
  `nama_matkul` varchar(60) NOT NULL,
  `mahasiswa_username` varchar(12) NOT NULL,
  PRIMARY KEY (`id_matkul`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `matakuliah`
--

INSERT INTO `matakuliah` (`id_matkul`, `dosen_username`, `nama_matkul`, `mahasiswa_username`) VALUES
(001, '1989091120180310', 'Pemrograman Web - A', '160533611400');

-- --------------------------------------------------------

--
-- Table structure for table `nilai`
--

CREATE TABLE IF NOT EXISTS `nilai` (
  `id_nilai` int(3) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `id_mahasiswa` varchar(12) NOT NULL,
  `id_tugas` int(3) unsigned zerofill NOT NULL,
  `nilai` int(3) NOT NULL,
  `waktu_nilai` datetime NOT NULL,
  PRIMARY KEY (`id_nilai`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tugas`
--

CREATE TABLE IF NOT EXISTS `tugas` (
  `id_tugas` int(3) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `id_matkul` int(3) unsigned zerofill NOT NULL,
  `nama_tugas` varchar(50) NOT NULL,
  `format_file` varchar(40) NOT NULL,
  `format_nama` varchar(40) NOT NULL,
  `deskripsi` text NOT NULL,
  `waktu_awal` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `waktu_akhir` datetime NOT NULL,
  PRIMARY KEY (`id_tugas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
