-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 28, 2018 at 08:52 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `simpelas`
--
CREATE DATABASE IF NOT EXISTS `simpelas` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `simpelas`;

-- --------------------------------------------------------

--
-- Table structure for table `admin_login`
--

DROP TABLE IF EXISTS `admin_login`;
CREATE TABLE IF NOT EXISTS `admin_login` (
  `admin_username` varchar(20) NOT NULL,
  `admin_password` text NOT NULL,
  PRIMARY KEY (`admin_username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_login`
--

INSERT INTO `admin_login` (`admin_username`, `admin_password`) VALUES
('admin', 'c4ca4238a0b923820dcc509a6f75849b');

-- --------------------------------------------------------

--
-- Table structure for table `ambil_matakuliah`
--

DROP TABLE IF EXISTS `ambil_matakuliah`;
CREATE TABLE IF NOT EXISTS `ambil_matakuliah` (
  `id_ambil` int(3) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `id_matkul` int(3) unsigned zerofill NOT NULL,
  `mahasiswa_username` varchar(12) NOT NULL,
  PRIMARY KEY (`id_ambil`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `ambil_matakuliah`
--

INSERT INTO `ambil_matakuliah` (`id_ambil`, `id_matkul`, `mahasiswa_username`) VALUES
(002, 004, '160533611470'),
(004, 005, '160533611470');

-- --------------------------------------------------------

--
-- Table structure for table `dosen_login`
--

DROP TABLE IF EXISTS `dosen_login`;
CREATE TABLE IF NOT EXISTS `dosen_login` (
  `dosen_username` varchar(18) NOT NULL,
  `dosen_password` text NOT NULL,
  `dosen_nama` varchar(60) NOT NULL,
  `dosen_email` varchar(70) NOT NULL,
  `dosen_notelp` varchar(16) NOT NULL DEFAULT '+62800000000',
  PRIMARY KEY (`dosen_username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dosen_login`
--

INSERT INTO `dosen_login` (`dosen_username`, `dosen_password`, `dosen_nama`, `dosen_email`, `dosen_notelp`) VALUES
('195105051982031001', 'c4ca4238a0b923820dcc509a6f75849b', 'Sujono', 'sujono@gmail.com', '+6288888888888'),
('198909112018031002', 'c4ca4238a0b923820dcc509a6f75849b', 'Wahyu Nur Hidayat', 'wahyu@gmail.com', '+6288888888888');

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa_login`
--

DROP TABLE IF EXISTS `mahasiswa_login`;
CREATE TABLE IF NOT EXISTS `mahasiswa_login` (
  `mahasiswa_username` varchar(12) NOT NULL,
  `mahasiswa_password` text NOT NULL,
  `mahasiswa_nama` varchar(60) NOT NULL,
  `mahasiswa_email` varchar(70) NOT NULL,
  `mahasiswa_notelp` varchar(16) NOT NULL DEFAULT '+62800000000',
  PRIMARY KEY (`mahasiswa_username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mahasiswa_login`
--

INSERT INTO `mahasiswa_login` (`mahasiswa_username`, `mahasiswa_password`, `mahasiswa_nama`, `mahasiswa_email`, `mahasiswa_notelp`) VALUES
('160533611401', 'c4ca4238a0b923820dcc509a6f75849b', 'Martha Devi Indraswari', 'marthadev@gmail.com', '+628181818181818'),
('160533611415', 'c4ca4238a0b923820dcc509a6f75849b', 'Livfia Alliv Lailla', 'livfia@gmail.com', '+628181818181818'),
('160533611470', 'c4ca4238a0b923820dcc509a6f75849b', 'Rendy Yofana', 'rendyyofanakusuma@gmail.com', '+628181818181818'),
('160533611504', 'c4ca4238a0b923820dcc509a6f75849b', 'Ismatul Izza', 'ismatulizza@gmail.com', '+628181818181818');

-- --------------------------------------------------------

--
-- Table structure for table `matakuliah`
--

DROP TABLE IF EXISTS `matakuliah`;
CREATE TABLE IF NOT EXISTS `matakuliah` (
  `id_matkul` int(3) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `dosen_username` varchar(18) NOT NULL,
  `nama_matkul` varchar(60) NOT NULL,
  PRIMARY KEY (`id_matkul`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `matakuliah`
--

INSERT INTO `matakuliah` (`id_matkul`, `dosen_username`, `nama_matkul`) VALUES
(004, '198909112018031002', 'Kewirausahaan - Pak Wahyu'),
(005, '195105051982031001', 'Elektro');

-- --------------------------------------------------------

--
-- Table structure for table `tugas`
--

DROP TABLE IF EXISTS `tugas`;
CREATE TABLE IF NOT EXISTS `tugas` (
  `id_tugas` int(3) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `id_matkul` int(3) unsigned zerofill NOT NULL,
  `nama_tugas` varchar(50) NOT NULL,
  `format_file` text NOT NULL,
  `format_nama_delimiter` varchar(1) NOT NULL,
  `format_nama` text,
  `deskripsi` text NOT NULL,
  `waktu_awal` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `waktu_akhir` datetime NOT NULL,
  PRIMARY KEY (`id_tugas`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tugas`
--

INSERT INTO `tugas` (`id_tugas`, `id_matkul`, `nama_tugas`, `format_file`, `format_nama_delimiter`, `format_nama`, `deskripsi`, `waktu_awal`, `waktu_akhir`) VALUES
(004, 004, 'KWU Gambar', 'jpg|jpeg|png|pdf|doc|docx|xls|xlsx', '-', 'mahasiswa_username-nama_tugas', 'Ini tugas', '2018-11-27 14:02:26', '2018-11-27 14:05:00'),
(005, 004, 'Tugas baru lagi', 'jpg|jpeg|png', '-', 'mahasiswa_nama-mahasiswa_username', 'Ini tugas kedua', '2018-11-28 10:22:37', '2018-12-07 10:05:00'),
(006, 005, 'Tugas Elektro', 'jpg|jpeg|png', '-', 'mahasiswa_nama-mahasiswa_username', 'Inilah tugas elektro yang kalian nanti', '2018-11-28 10:43:29', '2018-12-01 10:10:00');

-- --------------------------------------------------------

--
-- Table structure for table `tugas_kumpul`
--

DROP TABLE IF EXISTS `tugas_kumpul`;
CREATE TABLE IF NOT EXISTS `tugas_kumpul` (
  `id_kumpul` int(3) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `id_tugas` int(3) unsigned zerofill NOT NULL,
  `mahasiswa_username` varchar(12) NOT NULL,
  `nama_file` text NOT NULL,
  `nilai` int(3) NOT NULL DEFAULT '0',
  `waktu_upload` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `waktu_nilai` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_kumpul`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tugas_kumpul`
--

INSERT INTO `tugas_kumpul` (`id_kumpul`, `id_tugas`, `mahasiswa_username`, `nama_file`, `nilai`, `waktu_upload`, `waktu_nilai`) VALUES
(002, 004, '160533611470', '160533611470-KWUGambar.png', 100, '2018-11-28 14:24:57', '2018-11-28 14:24:57');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
