-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 09, 2018 at 10:51 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `simpelas`
--

-- --------------------------------------------------------

--
-- Table structure for table `daftar_pj`
--

CREATE TABLE IF NOT EXISTS `daftar_pj` (
  `id_daftar_pj` int(3) unsigned zerofill NOT NULL,
  `pj_username` varchar(12) NOT NULL,
  `id_matkul` int(3) unsigned zerofill NOT NULL,
  PRIMARY KEY (`id_daftar_pj`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dosen_login`
--

CREATE TABLE IF NOT EXISTS `dosen_login` (
  `dosen_username` varchar(18) NOT NULL,
  `dosen_password` text NOT NULL,
  PRIMARY KEY (`dosen_username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dosen_login`
--

INSERT INTO `dosen_login` (`dosen_username`, `dosen_password`) VALUES
('1989091120180310', '1');

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa_login`
--

CREATE TABLE IF NOT EXISTS `mahasiswa_login` (
  `mahasiswa_username` varchar(12) NOT NULL,
  `mahasiswa_password` text NOT NULL,
  PRIMARY KEY (`mahasiswa_username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mahasiswa_login`
--

INSERT INTO `mahasiswa_login` (`mahasiswa_username`, `mahasiswa_password`) VALUES
('160533611470', '1');

-- --------------------------------------------------------

--
-- Table structure for table `matakuliah`
--

CREATE TABLE IF NOT EXISTS `matakuliah` (
  `id_matkul` int(3) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `dosen_username` varchar(18) NOT NULL,
  `nama_matkul` varchar(60) NOT NULL,
  `pj_username` varchar(12) NOT NULL,
  PRIMARY KEY (`id_matkul`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `nilai`
--

CREATE TABLE IF NOT EXISTS `nilai` (
  `id_nilai` int(3) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `id_mahasiswa` varchar(12) NOT NULL,
  `id_tugas` int(3) unsigned zerofill NOT NULL,
  `nilai` int(3) NOT NULL,
  `waktu_nilai` datetime NOT NULL,
  PRIMARY KEY (`id_nilai`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pj_login`
--

CREATE TABLE IF NOT EXISTS `pj_login` (
  `pj_username` varchar(12) NOT NULL,
  `pj_password` text NOT NULL,
  PRIMARY KEY (`pj_username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pj_login`
--

INSERT INTO `pj_login` (`pj_username`, `pj_password`) VALUES
('160533611470', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tugas`
--

CREATE TABLE IF NOT EXISTS `tugas` (
  `id_tugas` int(3) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `id_matkul` int(3) unsigned zerofill NOT NULL,
  `nama_tugas` varchar(50) NOT NULL,
  `format_tugas` varchar(4) NOT NULL,
  `deskripsi` text NOT NULL,
  `waktu_awal` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `waktu_akhir` datetime NOT NULL,
  PRIMARY KEY (`id_tugas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
