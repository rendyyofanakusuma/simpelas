<?php if(!defined('BASEPATH')) exit('No direct script access allowed!');

class Model_security extends CI_Model{
	public function logged_in($akses){
		if($this->session->userdata("{$akses}_logged_in") OR $this->input->cookie("{$akses}_remember_token")){
			//recreate session after exiting browser e
			if($this->input->cookie("{$akses}_remember_token")){
				$this->session->set_userdata("{$akses}_logged_in",$this->input->cookie("{$akses}_remember_token", TRUE));
				$this->session->set_userdata("{$akses}_username",$this->input->cookie("{$akses}_dt1", TRUE));
				$this->session->set_userdata("{$akses}_password",$this->input->cookie("{$akses}dt2", TRUE));
			}
			return true;
		}else{
			alert_danger('Error', 'Anda harus login!');
			redirect(base_url("login/{$akses}"));
		}
	}


	public function batch_cookies_set($name, $val){
		$cookie = array(
		    'name'   => "{$name}",
		    'value'  => "{$val}",
		    'expire' => "1209600",  // Dua Minggu
		    'domain' => "localhost",
		    'path'	 => "/",
		);

		$this->input->set_cookie($cookie);
	}
	public function batch_delete_cookies($akses="mahasiswa"){
		$akses = strtolower($akses);
		delete_cookie("{$akses}_remember_token");
		delete_cookie("{$akses}_dt1");
		delete_cookie("{$akses}_dt2");
	}
}
?>