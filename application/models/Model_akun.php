<?php if(!defined('BASEPATH')) exit('No direct script access allowed!');

class Model_akun extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }

	// LOGIN
	function do_login($username, $password, $akses)
	{
		$cred = array(
			"{$akses}_username" => $username,
			"{$akses}_password" => $password,
		);
		$this->db->select("*");
		$this->db->from("{$akses}_login");
		$this->db->where($cred);

		$query = $this->db->get();
		$exist = $query->num_rows();

		if ($exist > 0 AND $exist == 1) {
			return $query->result();
		} else {
			return false;
		}
	}
	// SIGNUP
	function do_signup($data_insert)
	{
    	$this->db->trans_start();
    	$this->db->insert("mahasiswa_login", $data_insert);
		$this->db->trans_complete();

		return $this->db->trans_status();
	}

	function data_login($username, $password, $akses)
	{
		//sesuaikan database
		$cred = array(
			"{$akses}_username" => $username,
			"{$akses}_password" => $password,
		);
		$this->db->select("*");
		$this->db->from("{$akses}_login");
		$this->db->where($cred);

		$query = $this->db->get();
		return $query->row();
	}
}
?>