<?php if(!defined('BASEPATH')) exit('No direct script access allowed!');

class Model_link_list extends CI_Model{
	public function link_list($akses=""){
		if($akses!=""){
			switch($akses):
				case "admin":
					$ll = array(
						"home"					=>	"<i class='fa fa-home fa-fw'></i> Beranda",
						"dosen/daftar"			=>	array(
							"title" => "<i class='fa fa-chalkboard-teacher fa-fw'></i> Dosen",
							"dosen/daftar" => "Daftar Dosen",
							"dosen/tambah" => "Tambah Dosen",
						),
						"mahasiswa/daftar"			=>	array(
							"title" => "<i class='fa fa-user-graduate fa-fw'></i> Mahasiswa",
							"mahasiswa/daftar" => "Daftar mahasiswa",
							"mahasiswa/tambah" => "Tambah mahasiswa",
						),
						"matakuliah/daftar"			=>	array(
							"title" => "<i class='fa fa-book-open fa-fw'></i> Matakuliah",
							"matakuliah/daftar" => "Daftar Matakuliah",
							"matakuliah/tambah" => "Tambah Matakuliah",
						),
						"tugas/daftar"			=>	array(
							"title" => "<i class='fa fa-file-invoice fa-fw'></i> Tugas",
							"tugas/daftar" => "Daftar Tugas",
							"tugas/tambah" => "Tambah Tugas",
						),
						"shutdown"				=>	"<i class='fa fa-exclamation-triangle fa-fw'></i> Hapus Semua Data",
						"logout"				=>	"<i class='fa fa-sign-out-alt fa-fw'></i> Logout",
					);
				break;
				case "dosen":
					$ll = array(
						"home"					=>	"<i class='fa fa-home fa-fw'></i> Beranda",
						"dosen/daftar"			=>	array(
							"title" => "<i class='fa fa-chalkboard-teacher fa-fw'></i> Dosen",
							"dosen/daftar" => "Daftar Dosen",
						),
						"mahasiswa/daftar"			=>	array(
							"title" => "<i class='fa fa-user-graduate fa-fw'></i> Mahasiswa",
							"mahasiswa/daftar" => "Daftar mahasiswa",
						),
						"matakuliah/daftar"			=>	array(
							"title" => "<i class='fa fa-book-open fa-fw'></i> Matakuliah",
							"matakuliah/daftar" => "Daftar Matakuliah",
						),
						"tugas/daftar"			=>	array(
							"title" => "<i class='fa fa-file-invoice fa-fw'></i> Tugas",
							"tugas/daftar" => "Daftar Tugas",
							"tugas/tambah" => "Tambah Tugas",
						),
						"logout"				=>	"<i class='fa fa-sign-out-alt fa-fw'></i> Logout",
					);
				break;
				case "mahasiswa":
					$ll = array(
						"home"					=>	"<i class='fa fa-home fa-fw'></i> Beranda",
						"dosen/daftar"			=>	"<i class='fa fa-chalkboard-teacher fa-fw'></i> Dosen",
						"matakuliah/daftar"		=>	"<i class='fa fa-book-open fa-fw'></i> Matakuliah",
						"tugas/daftar"			=>	"<i class='fa fa-file-invoice fa-fw'></i> Tugas",
						"logout"				=>	"<i class='fa fa-sign-out-alt fa-fw'></i> Logout",
					);
				break;
			endswitch;
			return $ll;	
		}
	}
}
?>