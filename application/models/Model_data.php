<?php if(!defined('BASEPATH')) exit('No direct script access allowed!');

class Model_data extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }

    function fetch_column_name($tabel="", $column="*"){
        if($column == "*")
        	return $this->db->list_fields($tabel);
        else
        	return $column;
    }

    function fetch_column_PK($tabel=""){
		$fields = $this->db->field_data($tabel);
		foreach ($fields as $field){
			if($field->primary_key)
				return $field->name;
		}
    }

    //Default
	function fetch_default($tabel="", $cred="", $equal = "=", $column = "*", $join = "", $group="", $wherein=""){
		//sesuaikan database
		$this->db->select($column);
		$this->db->from($tabel);
		if(isset($cred) AND $cred!=""){
			if($equal == "="){
				if(isset($wherein) AND $wherein!=""){
					$this->db->where_in($wherein, $cred);
				}else{					
					$this->db->where($cred);
				}
			}
			else if($equal == "LIKE"){
				$this->db->like($cred);
			}
		}
		if(isset($join) AND $join!=""){
			foreach($join as $j=>$val)
				$this->db->join($val[0],$val[1],$val[2]);
		}
		if(isset($group) AND $group!=""){
			foreach($group as $g=>$val)
				$this->db->group_by($val[0]);
		}

		$query = $this->db->get();
		return $query->result();
	}

	function insert_default($tabel="", $data="", $batch=""){
    	$this->db->trans_start();
    	if($batch=="")
    		$this->db->insert("{$tabel}", $data);
    	else if ($batch=="batch")
    		$this->db->insert_batch("{$tabel}", $data);
		$this->db->trans_complete();

		return $this->db->trans_status();

	}
	function update_default($tabel="", $set="", $cred="", $batch=""){
    	$this->db->trans_start();
		if(isset($batch) AND $batch!=""){
			if($batch == "batch")
				$this->db->update_batch("{$tabel}", $set, $cred); 
		}else{
	        $this->db->set($set);
	        $this->db->where($cred);
			$this->db->update("{$tabel}");
		}
		$this->db->trans_complete();

		return $this->db->trans_status();
    }
	function delete_default($tabel="", $cred=""){
    	$this->db->trans_start();
        $this->db->where($cred);
        $this->db->delete($tabel);
		$this->db->trans_complete();

		return $this->db->trans_status();
	}

	function shutdown(){
		$tables=$this->db->query("SELECT t.TABLE_NAME AS myTables FROM INFORMATION_SCHEMA.TABLES AS t WHERE t.TABLE_SCHEMA = '".$this->db->database."'")->result_array();
    	$this->db->trans_start();
		foreach($tables as $t) {
			if ($t["myTables"] != 'admin_login'){
				$this->db->empty_table($t["myTables"]);
			}
		}
		$this->db->trans_complete();

		return $this->db->trans_status();
	}
}
?>