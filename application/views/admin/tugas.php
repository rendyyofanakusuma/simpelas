<?php
$this->load->view("inc/header");
$this->load->view("{$akses}/inc/header");
 ?>

<?php
if(isset(explode("/",$slugLoaded)[1])){
	$tipe_halaman = explode("/",$slugLoaded)[1];
	switch($tipe_halaman):
		case 'daftar':	?>
			<div class="card card-body bg-light mb-5 py-2">
				<div class="row d-flex justify-content-center btn-group" role="group">
					<a href="<?=base_url($akses.'/'.$pageName.'/tambah')?>" class="btn btn-success col-md-12"><i class="fa fa-plus-square"></i> Tambah <?=ucwords($pageName)?>
					</a>
				</div>
			</div>
			<div class="container-fluid">
				<table class="table table-bordered table-striped table-responsive" id="tabel_<?=$pageName?>">
					<thead>
					<tr align="center">
						<?php
						foreach($data_column as $data_column_key=>$data_column_val): ?>
							<th><span title="<?=$data_column_val?>"><?=(strlen($data_column_val) > 3) ? substr($data_column_val,0,3)."...".substr($data_column_val,strlen($data_column_val)-5,5) : $data_column_val;?></span></th>
						<?php
						endforeach;
						?>
						<th class="not-export-col">Aksi</th>
					</tr>
					</thead>
					<tbody>
					<?php
					if($data_tugas !==false):
						foreach($data_tugas as $data_key=>$data_val): ?>
							<tr>
								<?php
								foreach($data_column as $data_column_key=>$data_column_val): ?>
									<td><?=$data_val->$data_column_val;?></th>
								<?php
								endforeach;
								?>
								<td>
									<a href="<?=base_url($akses.'/'.$pageName.'/edit?'.$kolomID.'='.$data_val->$kolomID);?>"><button type="button" class="btn btn-warning btn-block">Edit</button></a>
									<a href="<?=base_url($akses.'/'.$pageName.'/hapus?'.$kolomID.'='.$data_val->$kolomID);?>" onclick="return(confirm('Yakin ingin menghapus?'))"><button type="button" class="btn btn-danger btn-block">Hapus</button></a>
									<a href="<?=base_url($akses.'/'.$pageName.'/upload?'.$kolomID.'='.$data_val->$kolomID);?>"><button type="button" class="btn btn-primary btn-block">Kirim Tugas</button></a>
									<a href="<?=base_url($akses.'/'.$pageName.'/daftarfile?'.$kolomID.'='.$data_val->$kolomID);?>"><button type="button" class="btn btn-dark btn-block">Daftar File Tugas <span class="badge"><?=$data_val->jumlah_file?></span></button></a>
								</td>
							</tr>
						<?php
						endforeach;
					endif;
					?>
					</tbody>
				</table>
			</div>
			<?php
			break;
		case 'daftarfile': ?>
			<div class="container-fluid">
				<form action="<?=base_url($akses.'/'.$pageName.'/daftarfile_act?act=nilai');?>" enctype="multipart/form-data" method="POST" data-parsley-validate>
				<input type="hidden" name="id_tugas" readonly value="<?=$this->input->get('id_tugas')?>" data-parsley-type="number">
				<table class="table table-bordered table-striped dt-responsive w-100" id="tabel_<?=$pageName?>">
					<thead>
						<tr>
						<?php foreach($data_column as $data_column_key=>$data_column_val):?>
							<th><span title="<?=$data_column_val?>"><?=$data_column_val?></span></th>
						<?php endforeach;?>
							<th class="not-export-col">Aksi</th>
						</tr>
					</thead>
					<tbody>
					<?php
					if($data_tugas_kumpul!==false):
						foreach($data_tugas_kumpul as $key=>$val): ?>
						<tr>
						<?php foreach($data_column as $data_column_key=>$data_column_val):?>
							<td><?=$val->$data_column_val?></td>
						<?php endforeach;?>
							<td>
								<input type="hidden" class="" name="batch_id_kumpul[]" readonly value="<?=$val->id_kumpul?>" data-parsley-type="number">
								<span>Nilai skala 0-100</span>
								<input type="text" class="form-control" class="" pattern="[*\d]{0,3}" maxlength="3" name="batch_nilai[<?=$val->id_kumpul?>]" value="<?=$val->nilai?>" data-parsley-type="number" data-parsley-min="0" data-parsley-max="100">
								<a href="<?=base_url('uploads/tugas/'.$this->input->get('id_tugas').'/'.urlencode($val->nama_file))?>" target="_blank"><button type="button" class="btn btn-primary btn-block">Buka File</button></a>
								<a href="<?=base_url($akses.'/'.$pageName.'/daftarfile_act?act=hapus&id_kumpul='.$val->id_kumpul);?>" onclick="return(confirm('Yakin ingin menghapus?'))"><button type="button" class="btn btn-danger btn-block">Hapus</button></a>
							</td>
						</tr>
						<?php
						endforeach;
					endif;
					?>
					</tbody>
				</table>
				<?php if(is_array($data_tugas_kumpul) || $data_tugas_kumpul instanceof Countable): ?>
				<button class="btn btn-danger btn-block" type="submit">Batch Update Nilai</button>
				<?php endif; ?>
				</form>
			</div>
		<?php
		break;
		case 'tambah':
		case 'edit':
			if(isset($data_tugas)):
				foreach($data_tugas as $val)
					$recent_mk = $val->id_matkul;
			endif;
			?>
			<form action="<?=base_url($akses.'/'.$pageName.'/'.$tipe_halaman.'_act')?>" class="form-login col-md-6" id="form-login" enctype="multipart/form-data" method="POST" data-parsley-validate>
				<?php
			if(isset($data_tugas)):?>
				<input type="hidden" name="id_tugas" value='<?="{$data_tugas[0]->id_tugas}"?>' readonly>
			<?php endif;?>
				<div class="form-group">
					<select name="inputMatkul" id="inputMatkul" class="form-control" required>
						<?php 
						foreach($data_matkul as $val): ?>
						<option value="<?=$val->id_matkul?>" <?=(isset($data_tugas)) ? (($recent_mk == $val->id_matkul) ? "selected" : "") : "" ?>><?="{$val->nama_matkul}"?></option>
						<?php endforeach; ?>
					</select>
				</div>
				<div class="form-label-group">
					<input type="text" id="inputNamaTugas" name="inputNamaTugas" class="form-control" placeholder="Nama Tugas" required maxlength="50" minlength="3" <?=(isset($data_tugas) ? " value='{$data_tugas[0]->nama_tugas}'" : "" )?> required>
					<label for="inputNamaTugas">Nama Tugas</label>
				</div>
				<div class="form-group">
					<label for="inputFFile">Format File Tugas</label>
					<?php $i = 1;
					foreach($FFile as $kata=>$format):?>
					<div class="custom-control custom-checkbox">
						<input type="checkbox" class="custom-control-input" name="inputFFile[]" id="inputFFile<?=$i?>" value="<?=$format;?>"<?= ($i==1) ? " data-parsley-required='true'" : "" ?> data-parsley-group="inputFFile" <?=(isset($data_tugas)) ? (strpos($data_tugas[0]->format_file,$format)!==false ? "checked" : "") : "" ?>>
						<label class="custom-control-label" for="inputFFile<?=$i++?>"><?="{$kata}"?></label>
					</div>
				<?php endforeach; ?>
				</div>
				<div class="form-group">
					<label for="inputFNamaDelimiter">Delimiter Format Nama</label>
					<input type="text" name="inputFNamaDelimiter" class="form-control" maxlength="1" minlength="1" size="1" required pattern="[^A-z0-9À-ž_#]|[\s]{1,1}" <?=(isset($data_tugas) ? " value='{$data_tugas[0]->format_nama_delimiter}'" : "" )?>>
				</div>
				<div class="form-group">
					<label for="inputFNama">Template Nama Yang Dimasukkan</label>
					<?php $i = 1;
					foreach($FNama as $kata=>$format):?>
					<div class="custom-control custom-checkbox">
						<input type="checkbox" class="custom-control-input" name="inputFNama[]" id="inputFNama<?=$i?>" value="<?=$format;?>" data-parsley-group="inputFNama" <?=(isset($data_tugas)) ? (strpos($data_tugas[0]->format_nama,$format)!==false ? "checked" : "") : "" ?>>
						<label class="custom-control-label" for="inputFNama<?=$i++?>"><?="{$kata}"?></label>
					</div>
				<?php endforeach; ?>
				</div>
				<div class="form-group">
					<label for="inputDeskripsi">Deskripsi Tugas</label>
					<textarea name="inputDeskripsi" id="inputDeskripsi" class="form-control"><?=(isset($data_tugas) ? "{$data_tugas[0]->deskripsi}" : "" )?></textarea>
				</div> 
				<div class="form-group">
					<label for="inputWaktuAkhir">Waktu Pengumpulan Terakhir</label>
					<input type="text" name="inputWaktuAkhir" id="inputWaktuAkhir" class="form-control" <?=(isset($data_tugas) ? " value='{$data_tugas[0]->waktu_akhir}'" : "" )?> required readonly pattern="[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}">
				</div>
				<div class="btn-group special btn-group-lg text-center" role="group" aria-label="<?=ucwords($tipe_halaman)?> Action">
					<button class="btn btn-warning" type="reset"><i class="fa fa-undo"></i>Reset</button>
					<button class="btn btn-primary" type="submit"><i class="fa fa-<?=(!isset($data_tugas) ? "plus-square" : "edit" )?>"></i><?=ucwords($tipe_halaman)?> <?=ucwords($pageName)?></button>
				</div>
			</form>
			<script type="text/javascript">
				<?php
				// Atur Tanggal Minimal, harus lebih 5 menit dibanding waktu sekarang
				$date = new DateTime(date("Y-m-d H:i"));
				$date->modify("+5 minute");
				$dateIncrement = $date->format("Y-m-d H:i:s");
				?>
				$('#inputWaktuAkhir').datetimepicker({
					format: "yyyy-mm-dd hh:ii:ss",
					language: "id",
					startDate: "<?=$dateIncrement;?>",
					clearBtn: true,
					todayHighlight: true,
					pickerPosition: 'top-right',
					fontAwesome: true,
					wheelViewModeNavigation: true,
					wheelViewModeNavigationInverseDirection: true,
					wheelViewModeNavigationDelay: 100,
				});
			</script>
			<?php
		break;
		case "upload":
			if(!isset($data_tugas))
				redirect(base_url("{$akses}/"));
			?>
			<div class="card">
				<div class="card-body">
					<h1 class="text-center">Silahkan upload - <?=$data_tugas[0]->nama_tugas?></h1>
				</div>
			</div>
			<div class="col-md-6 mx-auto">
				<div class="card">
					<div class="card-body">							
						<h5 class="card-title">Ketentuan</h5>
						<table class="card-text">
							<?php foreach ($data_tugas[0] as $key => $value):
								if(strpos($key,"id_")===false ):?>
								<tr>
									<td>
										<?=ucwords(str_replace("_", " ", $key))?>
									</td>
									<td>
										:
									</td>
									<td>
										<?=$value?>
									</td>
								</tr>							
								<?php
								endif;
							endforeach ?>
						</table>
					</div>
				</div>
			</div>
			<form action="<?=base_url($akses.'/'.$pageName.'/'.$tipe_halaman.'_act')?>" class="form-login col-md-6" id="form-login" enctype="multipart/form-data" method="POST" data-parsley-validate>
				<?php if(isset($data_tugas)): ?>
				<input type="hidden" name="id_tugas" value='<?="{$data_tugas[0]->id_tugas}"?>' readonly>
				<?php endif;?>
				<div class="form-group">
					<label for="inputNIM">Mahasiswa</label>
					<select class="form-control" name="inputNIM">
						<?php
						//fetch data mahasiswa yang ambil mk
						foreach($data_mahasiswa as $data_mahasiswa_val):
							?>
							<option value="<?=$data_mahasiswa_val->mahasiswa_username?>"><?="({$data_mahasiswa_val->mahasiswa_username}) {$data_mahasiswa_val->mahasiswa_nama}"?></option>
							<?php
						endforeach;
						?>
					</select>
				</div>
				<div class="custom-file">
					<input type="file" name="file_tugas_upload" class="custom-file-input" id="inputFile" accept="<?='.'.implode(',.', explode("|",$data_tugas[0]->format_file))?>" required>
					<label class="custom-file-label" for="inputFile">Pilih File</label>
				</div>
				<div class="btn-group special btn-group-lg text-center" role="group" aria-label="<?=ucwords($tipe_halaman)?> Action">
					<button class="btn btn-warning" type="reset"><i class="fa fa-undo-alt"></i> Reset</button>
					<button class="btn btn-primary" type="submit"><i class="fa fa-upload"></i> <?=ucwords($tipe_halaman)?> <?=ucwords($pageName)?></button>
				</div>
			</form>
		<?php
		break;
	endswitch;
}


$this->load->view("{$akses}/inc/footer");
$this->load->view("inc/footer"); ?>