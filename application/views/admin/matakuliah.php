<?php
$this->load->view("inc/header");
$this->load->view("{$akses}/inc/header");
 ?>
	
<?php
if(isset(explode("/",$slugLoaded)[1])){
	$tipe_halaman = explode("/",$slugLoaded)[1];
	switch($tipe_halaman):
		case 'daftar':?>
			<div class="card card-body bg-light mb-5 py-2">
				<div class="row d-flex justify-content-center btn-group" role="group">
					<a href="<?=base_url($akses.'/'.$pageName.'/tambah')?>" class="btn btn-success col-md-6">
						<i class="fa fa-plus-square"></i> Tambah <?=ucwords($pageName)?>
					</a>
					<a href="<?=base_url($akses.'/'.$pageName.'/mahasiswa/')?>" class="btn btn-info col-md-6">
						<i class="fa fa-check-square"></i> Mahasiswa Berdasarkan Matakuliah
					</a>
				</div>
			</div>
			<div class="container-fluid">
				<table class="table table-bordered table-striped dt-responsive w-100" id="tabel_<?=$pageName?>">
					<thead>
					<tr align="center">
						<?php
						foreach($data_column as $data_column_key=>$data_column_val): ?>
							<th><span title="<?=$data_column_val?>"><?=$data_column_val;?></span></th>
						<?php
						endforeach;
						?>
						<th class="not-export-col">Aksi</th>
					</tr>
					</thead>
					<tbody>
					<?php
					foreach($data_matkul as $data_key=>$data_val): ?>
						<tr>
							<?php
							foreach($data_column as $data_column_key=>$data_column_val): ?>
								<td><?=$data_val->$data_column_val;?></td>
							<?php
							endforeach;
							?>
							<td>
								<a href="<?=base_url($akses.'/'.$pageName.'/edit?'.$kolomID.'='.$data_val->$kolomID);?>"><button type="button" class="btn btn-warning btn-block">Edit</button></a>
								<a href="<?=base_url($akses.'/'.$pageName.'/hapus?'.$kolomID.'='.$data_val->$kolomID);?>" onclick="return(confirm('Yakin ingin menghapus?'))"><button type="button" class="btn btn-danger btn-block">Hapus</button></a>
							</td>
						</tr>
					<?php
					endforeach;
					?>
					</tbody>
				</table>
			</div>
			<?php
		break;
		case 'tambah':
		case 'edit':
			if(isset($data_matkul)):
				foreach($data_matkul as $dt_mk):
					$recent_dosen = $dt_mk->dosen_username;
				endforeach;
			endif;
			?>
			<form action="<?=base_url($akses.'/'.$pageName.'/'.$tipe_halaman.'_act')?>" class="form-login col-md-6" id="form-login" enctype="multipart/form-data" method="POST" data-parsley-validate>
				<div class="form-group">
					<label for="inputDosen">Dosen Pengampu</label>
					<select class="form-control" name="inputDosen">
						<?php
						//fetch data mahasiswa yang ambil mk
						foreach($data_dosen as $data_dosen_val):
							?>
							<option value="<?=$data_dosen_val->dosen_username?>" <?=(isset($data_matkul)) ? (($recent_dosen == $data_dosen_val->dosen_username) ? "selected" : "") : "" ?>><?="({$data_dosen_val->dosen_username}) {$data_dosen_val->dosen_nama}"?></option>
							<?php
						endforeach;
						?>
					</select>
				</div>
				<div class="form-label-group">
					<input type="text" id="inputNamaMatkul" name="inputNamaMatkul" class="form-control" placeholder="Nama Matakuliah" required " <?=(isset($data_matkul) ? "value='{$data_matkul[0]->nama_matkul}'" : "" )?>>
					<label for="inputNamaMatkul">Nama Matakuliah</label>
				</div>
				<?php
				if(isset($data_matkul)): ?>
					<input class="form-control" name="<?=$kolomID?>" type="text" readonly value="<?=($this->input->get($kolomID)) ? $this->input->get($kolomID) : '' ?>">
				<?php endif; ?>

				<div class="btn-group special btn-group-lg text-center" role="group" aria-label="Tambah Action">
					<button class="btn btn-warning" type="reset"><i class="fa fa-undo"></i> Reset</button>
					<button class="btn btn-primary" type="submit"><i class="fa fa-<?=(!isset($data_matkul) ? "plus-square" : "edit" )?>"></i> <?=ucwords($tipe_halaman)?>  <?=ucwords($pageName)?></button>
				</div>
			</form>
			<?php
		break;

		case 'mahasiswa':?>
		<form action="<?=base_url($akses.'/'.$slugLoaded.'_act')?>" method="post" enctype="multipart/form-data">
			<div class="card card-body bg-light mb-5 py-2">
				<div class="row d-flex justify-content-center btn-group" role="group">
					<select class="form-control" onchange="document.location='<?=base_url($akses.'/matakuliah/mahasiswa?id_matkul=')?>'+this.value" name="id_matkul">
					<?php foreach($data_matkul as $key=>$val):?>
						<option value="<?=$val->id_matkul?>"<?=($this->input->get("id_matkul")==$val->id_matkul ? " selected" : "" )?>><?="{$val->nama_matkul}"?></option>
					<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="container-fluid">
				<table class="table table-bordered table-striped dt-responsive w-100" id="tabel_ambilmk">
					<thead>
						<tr align="center">
							<?php
							foreach($data_column as $data_column_key=>$data_column_val):
								?>
								<th><span title="<?=$data_column_val?>"><?=$data_column_val;?></span></th>
							<?php
							endforeach;
							?>
							<th class="not-export-col">Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php
						foreach($data_mahasiswa as $data_key=>$data_val):
							?>
							<tr>
								<?php
								foreach($data_column as $data_column_key=>$data_column_val): ?>
								<td><?=$data_val->$data_column_val?></td>
								<?php
								endforeach;
								?>
								<td>
									<input type="checkbox" class="form-control" name="ambil_mk[<?=$data_val->mahasiswa_username?>]" id="ambilmk"<?=(in_array($data_val->mahasiswa_username, $data_ambil_mk)) ? " checked" : "" ?>>
								</td>
							</tr>
						<?php
						endforeach;
						?>
					</tbody>
				</table>
				<button type="submit" class="btn btn-warning btn-block">Edit Ambil Matakuliah</button>
			</div>
		</form>
		<?php
		break;
	endswitch;
}


$this->load->view("{$akses}/inc/footer");
$this->load->view("inc/footer"); ?>