<?php
$this->load->view("inc/header");
$this->load->view("{$akses}/inc/header");
 ?>
<div class="jumbotron jumbotron-fluid text-center">
	<div class="container">
		<h1 class="display-4">Dashboard "<?=strtoupper("{$akses}")?>"</h1>
		<p class="lead">Beberapa data penting terkait <?=$this->namaWeb?></p>
	</div>
</div>
<a href="<?=base_url("{$akses}/matakuliah")?>">
	<div class="card text-white text-center bg-success">
		<div class="card-header">Mata Kuliah yang Terdaftar</div>
		<div class="card-body">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-6 col-sm-12">
						<div><i class="fa fa-book-open fa-6x"></i></div>
					</div>
					<div class="col-md-6 col-sm-12">
						<h1 class="card-title"><?=count($data_matkul)?></h1>
						<p class="card-text" style="color:white !important;">buah</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</a>
<div id="chart1" class="col-md-6 col-sm-12 float-md-right float-sm-none"></div>
<div id="chart2" class="col-md-6 col-sm-12 float-md-left float-sm-none"></div>

<script type="text/javascript">
	$(function () {
	<?php if(isset($data_mahasiswa)): if($data_mahasiswa!=null): ?>
	var chart1;
		<?php
		//setup var 1
		$thn = date("Y");
		$arrThn = array();
		$data11 = array();
		for($i=$thn-5;$i<=$thn;$i++):
			$arrThn[]="'".$i."'";
		endfor;
		?>
		chart1 = Highcharts.chart('chart1', {
				chart: {
						type: 'spline'
				},
				title: {
						text: 'Mahasiswa yang Terdaftar'
				},
				xAxis: {
						categories: [
							<?php
							$arrThnIm = implode(",",$arrThn);
							echo $arrThnIm;
							?>
						]
				},
				yAxis: {
						title: {
								text: 'Jumlah Mahasiswa'
						}
				},        
				tooltip: {
						crosshairs: true,
						shared: true
				},
				series: [
					{
							name: 'Jumlah Mahasiswa',
							data: [
							<?php
							foreach($arrThn as $thnVal):
								$thnSmall = substr($thnVal,3,2);
								$data11[$thnSmall]=0;
								foreach($data_mahasiswa as $dtval11):
									if(substr($dtval11->mahasiswa_username,0,2) == $thnSmall){
										$data11[$thnSmall]+=1;
									}
								endforeach;
							endforeach;
							$data11 = implode(",",$data11);
							echo $data11;
							?>
							]
					},
				]
		});
	<?php endif; endif;?>
	<?php if(isset($data_dosen)): if($data_dosen!=null):?>
	var chart2;
		<?php
		//setup var 2
		$thn = date("Y");
		$arrThn = array();
		$data21 = array();
		for($i=$thn-5;$i<=$thn;$i++):
			$arrThn[]="'".$i."'";
		endfor;
		foreach($arrThn as $thnVal):
			$thnSmall = substr($thnVal,3,2);
			$data21[substr($thnVal,1,4)]=0;
			foreach($data_dosen as $dtval21):
				if(substr($dtval21->dosen_username,10,2) == $thnSmall){
					$data21[substr($thnVal,1,4)]+=1;
				}
			endforeach;
			if($data21[substr($thnVal,1,4)]!=0)
				$data21[substr($thnVal,1,4)] = (count($data_dosen)/$data21[substr($thnVal,1,4)])*100;
			else
				$data21[substr($thnVal,1,4)] = 0;
		endforeach;
		?>

		chart2 = Highcharts.chart('chart2', {
				chart: {
					type: 'pie'
				},
				title: {
						text: 'Dosen yang Terdaftar'
				},
				tooltip: {
						pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
				},
				//set di JS
				series: [{
					name: 'Jumlah Dosen',
					data : [
					<?php
					foreach($data21 as $key=>$val):
					?>{name: <?="'".$key."'"?>,y: <?=$val?>},<?php
					endforeach; ?>
					]
				}]
			});
	<?php endif; endif;?>
	});
</script>

<?php
$this->load->view("{$akses}/inc/footer");
$this->load->view("inc/footer"); ?>