<?php
$this->load->view("inc/header");
$this->load->view("{$akses}/inc/header");
 ?>
 
<?php
if(isset(explode("/",$slugLoaded)[1])){
	$tipe_halaman = explode("/",$slugLoaded)[1];
	switch($tipe_halaman):
		case 'daftar': ?>
			<div class="card card-body bg-light mb-5 py-2">
				<div class="row d-flex justify-content-center btn-group" role="group">
					<a href="<?=base_url($akses.'/'.$pageName.'/tambah')?>" class="btn btn-success col-md-6"><i class="fa fa-plus-square"></i> Tambah <?=ucwords($pageName)?>
					</a>
					<a href="<?=base_url($akses.'/matakuliah/mahasiswa/')?>" class="btn btn-info col-md-6">
						<i class="fa fa-check-square"></i> Mahasiswa Berdasarkan Matakuliah
					</a>
				</div>
			</div>
			<div class="container-fluid">
				<table class="table table-bordered table-striped dt-responsive w-100" id="tabel_<?=$pageName?>">
					<thead>
						<tr align="center">
							<?php
							foreach($data_column as $data_column_key=>$data_column_val):
								?>
								<th><span title="<?=$data_column_val?>"><?=$data_column_val;?></span></th>
							<?php
							endforeach;
							?>
							<th class="not-export-col">Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php
						foreach($data_mahasiswa as $data_key=>$data_val):
							?>
							<tr>
								<?php
								foreach($data_column as $data_column_key=>$data_column_val): ?>
								<td><?=(strlen($data_val->$data_column_val) > 10) ? "<span title='".$data_val->$data_column_val."'>".substr($data_val->$data_column_val,0,10)."...</span>" : $data_val->$data_column_val;?></td>
								<?php
								endforeach;
								?>
								<td>
									<a href="<?=base_url($akses.'/'.$pageName.'/edit?'.$kolomID.'='.$data_val->$kolomID);?>"><button type="button" class="btn btn-warning btn-block">Edit</button></a>
									<a href="<?=base_url($akses.'/'.$pageName.'/hapus?'.$kolomID.'='.$data_val->$kolomID);?>" onclick="return(confirm('Yakin ingin menghapus?'))"><button type="button" class="btn btn-danger btn-block">Hapus</button></a>
								</td>
							</tr>
						<?php
						endforeach;
						?>
					</tbody>
				</table>
			</div>
			<?php
		break;
		case 'tambah':
		case 'edit': ?>
			<form action="<?=base_url($akses.'/'.$pageName.'/'.$tipe_halaman.'_act')?>" class="form-login col-md-6" id="form-login" enctype="multipart/form-data" method="POST" data-parsley-validate>
				<div class="form-label-group">
					<input type="text" id="inputUsername" name="inputUsername" class="form-control" placeholder="NIM" required maxlength="12"minlength="12" pattern="[*\d]{12,12}" data-parsley-type="number" <?=(isset($data_mahasiswa) ? "readonly value='{$data_mahasiswa[0]->$data_column[0]}'" : "" )?>>
					<label for="inputUsername">NIM</label>
				</div>
				<div class="form-label-group">
					<input type="password" id="inputPassword1" name="inputPassword[]" class="form-control" placeholder="Password" required>
					<label for="inputPassword1">Password</label>
				</div>
				<div class="form-label-group">
					<input type="text" id="inputName" name="inputName[]" class="form-control" placeholder="Nama" required pattern="^[A-Za-z\s]+$" <?=(isset($data_mahasiswa) ? "value='{$data_mahasiswa[0]->$data_column[2]}'" : "" )?>>
					<label for="inputName">Nama Lengkap</label>
				</div>
				<div class="form-label-group">
					<input type="email" id="inputEmail1" name="inputEmail[]" class="form-control" placeholder="E-mail" required <?=(isset($data_mahasiswa) ? "value='{$data_mahasiswa[0]->$data_column[3]}'" : "" )?>>
					<label for="inputEmail1">E-mail</label>
				</div>
				<div class="form-label-group">
					<input type="text" id="inputNoTelp" name="inputNoTelp" class="form-control" placeholder="Nomor Telepon" required pattern="^(\+62)(\d{5,13})$" minlength="8" maxlength="16" <?=(isset($data_mahasiswa) ? "value='{$data_mahasiswa[0]->$data_column[4]}'" : "" )?>>
					<label for="inputNoTelp">Nomor Telepon (Format +62xxxx)</label>
				</div>
				<div class="btn-group special btn-group-lg text-center" role="group" aria-label="Tambah Action">
					<button class="btn btn-warning" type="reset"><i class="fa fa-undo"></i>Reset</button>
					<button class="btn btn-primary" type="submit"><i class="fa fa-<?=(!isset($data_mahasiswa) ? "plus-square" : "edit" )?>"></i><?=ucwords($tipe_halaman)?>  <?=ucwords($pageName)?></button>
				</div>
			</form>
			<?php
		break;
	endswitch;
} //penutup IF ISSET EXPLODE


$this->load->view("{$akses}/inc/footer");
$this->load->view("inc/footer"); ?>