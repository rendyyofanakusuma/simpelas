	</div>
</div>



<script type="text/javascript">
	$(document).ready(function () {
		$("#sidebar").mCustomScrollbar({
			theme: "minimal"
		});

		$('#dismiss, .overlay').on('click', function () {
			$('#sidebar').removeClass('active');
			$('.overlay').removeClass('active');
		});

		$('#sidebarCollapse').on('click', function () {
			$('#sidebar').addClass('active');
			$('.overlay').addClass('active');
			$('.collapse.in').toggleClass('in');
			$('a[aria-expanded=true]').attr('aria-expanded', 'false');
		});

		<?php if($pageName!='home'):
		if($slugLoaded=="matakuliah/mahasiswa"){
			$pageName="ambilmk"; ?>

			/* Create an array with the values of all the checkboxes in a column */
			$.fn.dataTable.ext.order['dom-checkbox'] = function  ( settings, col )
			{
				return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
					return $('input', td).prop('checked') ? '1' : '0';
				} );
			}
		<?php
		} ?>
		//DATATABEL
		var table = $('#tabel_<?=$pageName?>').DataTable({
			"language": {
				"url": "<?=base_url('assets/plugin/DataTables/Language/Indonesian.json')?>"
			},
			"columnDefs": [
				{  "targets": -1, "searchable":false,
				 <?=($pageName=="ambilmk") ? "'orderDataType': 'dom-checkbox'," : "'orderable': false,"?> 
				}
			],
			dom: 'Bfrtip',
			buttons: [
				<?php
				$arrExport = array('copy', 'csv', 'excel', 'pdf', 'print');
				foreach($arrExport as $item): ?>{
					extend: <?="'{$item}'"?>,
					exportOptions: {
						columns: ':visible:not(.not-export-col)'
					}
				},
				<?php endforeach; ?>
			],
			<?=($pageName=="ambilmk") ? "'columns': [
					null,
					null,
					{ 'orderData': [ 2,1,0 ] },
					]," : "" ?>
		});
		<?=($pageName=="ambilmk") ? "
			//sort based on checkbox
			$(':checkbox').on('change', function(e) {
				var row = $(this).closest('tr');
				var hmc = row.find(':checkbox:checked').length;
				var kluj = parseInt(hmc);
				row.find('td.counter').text(kluj);
				table.row(row).invalidate('dom');
			});" : "" ?>
		<?php endif; ?>
	});
</script>