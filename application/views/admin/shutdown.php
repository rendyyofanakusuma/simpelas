<?php
$this->load->view("inc/header");
$this->load->view("{$akses}/inc/header");
 ?>
 <div class="col-md-6 mx-auto text-center">
	Apakah anda yakin menghapus semua?<br>Silahkan isi dengan <span style="user-select:none;">"<?="{$konfirmasiText}"?>"</span>
	<form action="<?=base_url($akses.'/'.$pageName.'/shutdown_act')?>" enctype="multipart/form-data" method="POST" data-parsley-validate autocomplete="off" onsubmit="return confirm('Apakah anda benar-benar yakin?')">
		<div class="form-label-group">
			<input type="text" id="inputKonfirmasi" name="inputKonfirmasi" class="form-control text-center" placeholder="Masukkan kata validasi" required maxlength="<?=strlen($konfirmasiText)?>" minlength="<?=strlen($konfirmasiText)?>" data-parsley-m-validasi>
			<label for="inputKonfirmasi">Isi disini</label>
		</div>
		<button class="btn btn-danger btn-block" type="submit"><i class="fa fa-trash-alt"></i> Hapus Semua</button>
	</form>
</div>

<script type="text/javascript">
	window.Parsley.addValidator('mValidasi', {
	validateString: function(value, requirement) {
		if(value == '<?=$konfirmasiText?>')
		    return value;
		else 
		    return false;
		},
		requirementType: 'string',
		messages: {
			en: 'Fill with the correct word',
			id: 'Isi dengan benar',
		}
	});
</script>
<?php
$this->load->view("{$akses}/inc/footer");
$this->load->view("inc/footer"); ?>