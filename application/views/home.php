<?php $this->load->view("inc/header"); ?>
	<link rel="stylesheet" href="<?=base_url()?>assets/css/cover.css">
</head>
<body class="text-center" data-vide-bg="<?=base_url('assets/video/homebg')?>/vide" data-vide-options="loop: true, muted: true, position: 0% 0%, playbackRate: 1.75">
<div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
	<header class="masthead mb-auto">
		<div class="inner">
			<h3 class="masthead-brand"><?=$this->namaWeb;?></h3>
			<nav class="nav nav-masthead justify-content-center">
				<a class="nav-link active" href="#">Home</a>
				<a class="nav-link" href="<?=base_url('login')?>">Login</a>
				<a class="nav-link" href="#" data-toggle="modal" data-target="#about">About</a>
			</nav>
		</div>
	</header>

	<main role="main" class="inner cover">
		<h1 class="cover-heading"><?=$this->namaWeb;?></h1>
		<p class="lead"><?=$this->akronimWeb;?></p>
		<p class="lead">
			<a href="#" class="btn btn-lg btn-secondary" data-toggle="modal" data-target="#pelajari">Pelajari Lebih Lanjut</a>
		</p>
	</main>

	<footer class="mastfoot mt-auto">
		<div class="inner">
			<p>Cover template for <a href="https://getbootstrap.com/">Bootstrap</a>, by <a href="https://twitter.com/mdo">@mdo</a>.</p>
		</div>
	</footer>
</div>

<!-- Modal Pelajari -->
<div class="modal fade" id="pelajari" tabindex="-1" role="dialog" aria-labelledby="SIMPELAS" aria-hidden="true" style="color:black !important;">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="pelajariTitle"><?=$this->namaWeb;?> | <?=$this->akronimWeb;?></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p class="text-justify">
					<?=$this->namaWeb;?> adalah sebuah website yang menawarkan kemudahan dalam pengumpulan tugas secara kolektif dalam suatu kelas. Dengan adanya <?=$this->namaWeb;?> memungkinkan dosen untuk mengakses detail info tentang tugas yang telah dikumpulkan oleh mahasiswa. Tidak perlu bersusah payah untuk mencari mahasiswa mana yang belum mengumpulkan ataupun yang terlambat hingga melebihi deadline karena di dalam sistem akan secara otomatis terdeteksi. Kata <?=$this->namaWeb;?> sendiri diambil dari singkatan sistem pengumpulan tugas, kata tersebut dipilih karena sesuai dengan tujuan kami membuat sistem yaitu sederhana atau simple. Mahasiswa hanya perlu memasukkan file tugas ke dalam kolom yang sudah disediakan oleh penanggung jawab kelas. Dengan begitu data tugas akan tersimpan secara berurutan sehingga dapat dengan mudah diolah dan dinilai oleh dosen yang bersangkutan.
				</p>
			</div>
			<div class="modal-footer">
				Selamat bergabung di <?=$this->namaWeb;?>
			</div>
		</div>
	</div>
</div>


<!-- Modal About -->
<div class="modal fade" id="about" tabindex="-1" role="dialog" aria-labelledby="SIMPELAS" aria-hidden="true" style="color:black !important;">
	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="pelajariTitle">About <?=$this->namaWeb;?> | <?=$this->akronimWeb;?></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="container-fluid">
					<div class="row">
						<?php
						$pembuat = array(
							"160533611504" => "Ismatul Izza",
							"160533611415" => "LIVFIA ALLIV LAILLA", 
							"160533611401" => "MARTHA DEVI INDRASWARI", 
							"160533611470" => "Rendy Yofana Kusuma",
						);
						$colBagi = 12/count($pembuat);
						foreach($pembuat as $key=>$val):
						?>
						<div class="col-md-<?=$colBagi?> d-flex align-items-stretch">
							<div class="card">
								<img class="card-img-top" src="<?=base_url("assets/img/{$key}.jpg")?>" alt="Foto <?="{$key}"?>">
								<div class="card-body">
									<h5 class="card-title"><?=ucwords(strtolower($val))?></h5>
									<p class="card-text"><?="{$key}"?></p>
								</div>
							</div>
						</div>
						<?php
						endforeach;
						?>
					</div>
				</div>
			</div>
			<div class="modal-footer">
			</div>
		</div>
	</div>
</div>
<?php $this->load->view("inc/footer"); ?>
