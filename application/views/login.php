<?php $this->load->view("inc/header"); ?>
	<link rel="stylesheet" href="<?=base_url()?>assets/css/sign_in.css">
</head>
<body style="background:#a7bcff;">
	<form class="form-login" id="form-login" method="post" action="<?=base_url($pageName.'/'.$pageName.'_act/'.$akses)?>" enctype="multipart/form-data" data-parsley-validate>
		<div class="text-center mb-4">
			<img class="mb-4" src="<?=$this->ikonWeb?>" alt="" width="72" height="72">
			<h1 class="h3 mb-3 font-weight-normal">Selamat datang kembali di <?=$this->namaWeb?></h1>
			<p>Silahkan masukkan username dan password anda</p>
		</div>
		<?php echo $this->session->flashdata('alert'); ?>
		<div class="form-label-group">
			<input type="text" id="inputUsername" class="form-control" placeholder="Username / NIM" name="username" required autofocus maxlength="<?=$username_field_length_max;?>" minlength="<?=$username_field_length_min;?>" pattern="[<?=($akses!='admin')?'*\d':'\w';?>]{<?=$username_field_length_min;?>,<?=$username_field_length_max;?>}">
			<label for="inputUsername">Username <?=(isset($login_type_id) ? "/ ".$login_type_id : "" )?></label>
		</div>

		<div class="form-label-group">
			<input type="password" id="inputPassword" class="form-control" placeholder="Password" name="password" required>
			<label for="inputPassword">Password</label>
		</div>

		<div class="custom-control custom-checkbox">
			<input type="checkbox" class="custom-control-input" name="remember" id="remember">
			<label class="custom-control-label" for="remember">Ingat Saya</label>
		</div>
		<button class="btn btn-lg btn-<?=$btn_warna?> btn-block" type="submit">Login sebagai <?=strtoupper($akses)?></button>
		<?php
		if($akses == "mahasiswa"){?>
			<p class="text-center">Apabila belum terdaftar, <a href="<?=base_url('signup/'.$akses)?>">Sign Up</a> disini</p>
		<?php } ?>
		<p class="mt-5 mb-3 text-muted text-center">&copy; <?=date('Y')?></p>
	</form>

<?php $this->load->view("inc/footer"); ?>