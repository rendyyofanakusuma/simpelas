<?php
$this->load->view("inc/header");
$this->load->view("{$akses}/inc/header");
 ?>

<?php
if(isset(explode("/",$slugLoaded)[1])){
	$tipe_halaman = explode("/",$slugLoaded)[1];
	switch($tipe_halaman):
		case 'daftar':	?>
			<div class="container-fluid">
				<table class="table table-bordered table-striped table-responsive" id="tabel_<?=$pageName?>">
					<thead>
					<tr align="center">
						<?php
						foreach($data_column as $data_column_key=>$data_column_val): ?>
							<th><span title="<?=$data_column_val?>"><?=(strlen($data_column_val) > 3) ? substr($data_column_val,0,3)."...".substr($data_column_val,strlen($data_column_val)-5,5) : $data_column_val;?></span></th>
						<?php
						endforeach;
						?>
						<th class="not-export-col">Aksi</th>
					</tr>
					</thead>
					<tbody>
					<?php
					if($data_tugas !==false):
						foreach($data_tugas as $data_key=>$data_val): ?>
							<tr>
								<?php
								foreach($data_column as $data_column_key=>$data_column_val): ?>
									<td><?=$data_val->$data_column_val;?></th>
								<?php
								endforeach;
								?>
								<td>
									<a href="<?=base_url($akses.'/'.$pageName.'/upload?'.$kolomID.'='.$data_val->$kolomID);?>"><button type="button" class="btn btn-primary btn-block">Kirim Tugas</button></a>

									<?php
									foreach($data_tugas_kumpul as $data_tgk_key=>$data_tgk_val):
										if($data_tgk_val->id_tugas == $data_val->id_tugas AND $data_tgk_val->mahasiswa_username == $this->session->userdata("{$akses}_username")): ?>
										<a href="<?=base_url("uploads/tugas/{$data_val->id_tugas}/".urlencode("{$data_tgk_val->nama_file}"));?>" target="_blank"><button type="button" class="btn btn-success btn-block">Lihat Tugas Yang Dikirim</button></a>
										<div class="input-group">
											<div class="input-group-prepend">
												<div class="input-group-text">Nilai Anda</div>
											</div>
											<input type="text" value="<?=$data_tgk_val->nilai?>" readonly class="form-control text-center">
										</div>
										<?php
										endif;
									endforeach;?>
								</td>
							</tr>
						<?php
						endforeach;
					endif;
					?>
					</tbody>
				</table>
			</div>
			<?php
		break;
		case "upload":
			if(!isset($data_tugas))
				redirect(base_url("{$akses}/"));
			?>
			<div class="card">
				<div class="card-body">
					<h1 class="text-center">Silahkan upload - <?=$data_tugas[0]->nama_tugas?></h1>
				</div>
			</div>
			<div class="col-md-6 mx-auto">
				<div class="card">
					<div class="card-body">							
						<h5 class="card-title">Ketentuan</h5>
						<table class="card-text">
							<?php foreach ($data_tugas[0] as $key => $value):
								if(strpos($key,"id_")===false ):?>
								<tr>
									<td>
										<?=ucwords(str_replace("_", " ", $key))?>
									</td>
									<td>
										:
									</td>
									<td>
										<?=$value?>
									</td>
								</tr>							
								<?php
								endif;
							endforeach ?>
						</table>
					</div>
				</div>
			</div>
			<form action="<?=base_url($akses.'/'.$pageName.'/'.$tipe_halaman.'_act')?>" class="form-login col-md-6" id="form-login" enctype="multipart/form-data" method="POST" data-parsley-validate>
				<?php if(isset($data_tugas)): ?>
				<input type="hidden" name="id_tugas" value='<?="{$data_tugas[0]->id_tugas}"?>' readonly>
				<?php endif;?>
				<div class="custom-file">
					<input type="file" name="file_tugas_upload" class="custom-file-input" id="inputFile" accept="<?='.'.implode(',.', explode("|",$data_tugas[0]->format_file))?>" required>
					<label class="custom-file-label" for="inputFile">Pilih File</label>
				</div>
				<div class="btn-group special btn-group-lg text-center" role="group" aria-label="<?=ucwords($tipe_halaman)?> Action">
					<button class="btn btn-warning" type="reset"><i class="fa fa-undo-alt"></i> Reset</button>
					<button class="btn btn-primary" type="submit"><i class="fa fa-upload"></i> <?=ucwords($tipe_halaman)?> <?=ucwords($pageName)?></button>
				</div>
			</form>
		<?php
		break;
	endswitch;
}


$this->load->view("{$akses}/inc/footer");
$this->load->view("inc/footer"); ?>