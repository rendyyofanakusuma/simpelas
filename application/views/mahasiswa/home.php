<?php
$this->load->view("inc/header");
$this->load->view("{$akses}/inc/header");
 ?>
<div class="jumbotron jumbotron-fluid text-center">
	<div class="container">
		<h1 class="display-4">Dashboard "<?=strtoupper("{$akses}")?>"</h1>
		<p class="lead">Beberapa data penting terkait <?=$this->namaWeb?></p>
	</div>
</div>
<div class="container-fluid">
	<div class="row">
		<a href="<?=base_url("{$akses}/matakuliah")?>" class="col-md-4">
			<div class="card text-white text-center bg-success">
				<div class="card-header">Matakuliah yang Diikuti</div>
				<div class="card-body">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-6 col-sm-12">
								<div><i class="fa fa-book-open fa-6x"></i></div>
							</div>
							<div class="col-md-6 col-sm-12">
								<h1 class="card-title"><?=(is_array($data_matkul) || $data_matkul instanceof Countable) ? count($data_matkul) : "0" ?></h1>
								<p class="card-text" style="color:white !important;">buah</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</a>
		<a href="<?=base_url("{$akses}/tugas")?>" class="col-md-4">
			<div class="card text-center bg-warning">
				<div class="card-header">Tugas yang Perlu Dikumpulkan</div>
				<div class="card-body">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-6 col-sm-12">
								<div><i class="fa fa-file-invoice fa-6x"></i></div>
							</div>
							<div class="col-md-6 col-sm-12">
								<h1 class="card-title"><?=(is_array($data_tugas) || $data_tugas instanceof Countable) ? count($data_tugas) : "0" ?></h1>
								<p class="card-text" style="color:black !important;">buah</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</a>
		<a href="<?=base_url("{$akses}/tugas")?>" class="col-md-4">
			<div class="card text-white text-center bg-danger">
				<div class="card-header">Tugas yang Sudah Dikumpulkan</div>
				<div class="card-body">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-6 col-sm-12">
								<div><i class="fa fa-file-upload fa-6x"></i></div>
							</div>
							<div class="col-md-6 col-sm-12">
								<h1 class="card-title"><?=(is_array($data_tugas_kumpul) || $data_tugas_kumpul instanceof Countable) ? count($data_tugas_kumpul) : "0" ?></h1>
								<p class="card-text" style="color:white !important;">buah</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</a>		
	</div>
</div>

<?php
$this->load->view("{$akses}/inc/footer");
$this->load->view("inc/footer"); ?>