<?php
$this->load->view("inc/header");
$this->load->view("{$akses}/inc/header");
 ?>
	
<?php
if(isset(explode("/",$slugLoaded)[1])){
	$tipe_halaman = explode("/",$slugLoaded)[1];
	switch($tipe_halaman):
		case 'daftar': ?>
			<div class="container-fluid">
				<table class="table table-bordered table-striped dt-responsive w-100" id="tabel_<?=$pageName?>">
					<thead>
						<tr align="center">
							<?php
							foreach($data_column as $data_column_key=>$data_column_val):
								?>
								<th><span title="<?=$data_column_val?>"><?=$data_column_val;?></span></th>
							<?php
							endforeach;
							?>
							<th class="not-export-col"></th>
						</tr>
					</thead>
					<tbody>
						<?php
						foreach($data_dosen as $data_key=>$data_val):
							?>
							<tr>
								<?php
								foreach($data_column as $data_column_key=>$data_column_val): ?>
								<td><?=(strlen($data_val->$data_column_val) > 10) ? "<span title='".$data_val->$data_column_val."'>".substr($data_val->$data_column_val,0,10)."...</span>" : $data_val->$data_column_val;?></td>
								<?php
								endforeach;
								?>
								<td>
								</td>
							</tr>
						<?php
						endforeach;
						?>
					</tbody>
				</table>
			</div>
			<?php
		break;
	endswitch;
} //penutup IF ISSET EXPLODE

$this->load->view("{$akses}/inc/footer");
$this->load->view("inc/footer"); ?>