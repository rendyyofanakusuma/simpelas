		<!-- Sidebar CSS -->
		<link rel="stylesheet" href="<?=base_url()?>assets/css/sidebar.css">
		<link rel="stylesheet" href="<?=base_url()?>assets/css/<?=$akses;?>.css">
	</head>
	<body>
	<div class="overlay"></div>
	<div class="wrapper">

	<?php $this->load->view("/inc/sidebar");?>

		<div class="content">
		    <nav class="navbar navbar-expand-lg">
		        <div class="container-fluid">

		            <button type="button" id="sidebarCollapse" class="btn btn-info">
		                <i class="fas fa-align-justify"></i>
		                <span><?=substr(strtoupper("{$akses}"),0,3)?></span>
		            </button>
	                <nav aria-label="breadcrumb" class="float-right">
						<ol class="breadcrumb" style="margin-bottom:0;">
							<?php
							$linknya = base_url("{$akses}");
							$explodedLink = explode("/",$slugLoaded);
							$currentURL = $explodedLink[count($explodedLink)-1];
							foreach($explodedLink as $link):
								?>
								<?php $linknya .= "/".$link;?>
								<li class="breadcrumb-item <?=($currentURL==$link) ? 'active' : '';?>"><a href="<?=($currentURL==$link) ? '#' : $linknya ?>"><?=strtoupper($link)?></a></li>
								<?php
							endforeach;?>
						</ol>
					</nav>
		        </div>
		    </nav>

			<?php echo $this->session->flashdata("alert");?>