	</div>
</div>



<script type="text/javascript">
	$(document).ready(function () {
		$("#sidebar").mCustomScrollbar({
			theme: "minimal"
		});

		$('#dismiss, .overlay').on('click', function () {
			$('#sidebar').removeClass('active');
			$('.overlay').removeClass('active');
		});

		$('#sidebarCollapse').on('click', function () {
			$('#sidebar').addClass('active');
			$('.overlay').addClass('active');
			$('.collapse.in').toggleClass('in');
			$('a[aria-expanded=true]').attr('aria-expanded', 'false');
		});

		<?php if($pageName!='home'): ?>
		//DATATABEL
		var table = $('#tabel_<?=$pageName?>').DataTable({
			"language": {
				"url": "<?=base_url('assets/plugin/DataTables/Language/Indonesian.json')?>"
			},
			"columnDefs": [
				{  "targets": -1, "searchable":false,
				 <?=($pageName=="ambilmk") ? "'orderDataType': 'dom-checkbox'," : "'orderable': false,"?> 
				}
			],
			dom: 'Bfrtip',
			buttons: [
				<?php
				$arrExport = array('copy', 'csv', 'excel', 'pdf', 'print');
				foreach($arrExport as $item): ?>{
					extend: <?="'{$item}'"?>,
					exportOptions: {
						columns: ':visible:not(.not-export-col)'
					}
				},
				<?php endforeach; ?>
			],
			<?=($pageName=="ambilmk") ? "'columns': [
					null,
					null,
					{ 'orderData': [ 2,1,0 ] },
					]," : "" ?>
		});
		<?php endif; ?>
	});
</script>