<?php
$this->load->view("inc/header");
$this->load->view("{$akses}/inc/header");
 ?>
	
<?php
if(isset(explode("/",$slugLoaded)[1])){
	$tipe_halaman = explode("/",$slugLoaded)[1];
	switch($tipe_halaman):
		case 'daftar':?>
			<div class="container-fluid">
				<table class="table table-bordered table-striped dt-responsive w-100" id="tabel_<?=$pageName?>">
					<thead>
					<tr align="center">
						<?php
						foreach($data_column as $data_column_key=>$data_column_val): ?>
							<th><span title="<?=$data_column_val?>"><?=$data_column_val;?></span></th>
						<?php
						endforeach;
						?>
						<th class="not-export-col">Aksi</th>
					</tr>
					</thead>
					<tbody>
					<?php
					foreach($data_matkul as $data_key=>$data_val): ?>
						<tr>
							<?php
							foreach($data_column as $data_column_key=>$data_column_val): ?>
								<td><?=$data_val->$data_column_val;?></td>
							<?php
							endforeach;
							?>
							<td>
								<a href="<?=base_url($akses.'/tugas/daftar?id_matkul='.$data_val->id_matkul);?>"><button type="button" class="btn btn-primary btn-block">Daftar Tugas</button></a>
							</td>
						</tr>
					<?php
					endforeach;
					?>
					</tbody>
				</table>
			</div>
			<?php
		break;
	endswitch;
}


$this->load->view("{$akses}/inc/footer");
$this->load->view("inc/footer"); ?>