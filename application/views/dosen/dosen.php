<?php
$this->load->view("inc/header");
$this->load->view("{$akses}/inc/header");
 ?>
	
<?php
if(isset(explode("/",$slugLoaded)[1])){
	$tipe_halaman = explode("/",$slugLoaded)[1];
	switch($tipe_halaman):
		case 'daftar': ?>
			<div class="container-fluid">
				<table class="table table-bordered table-striped dt-responsive w-100" id="tabel_<?=$pageName?>">
					<thead>
						<tr align="center">
							<?php
							foreach($data_column as $data_column_key=>$data_column_val):
								?>
								<th><span title="<?=$data_column_val?>"><?=$data_column_val;?></span></th>
							<?php
							endforeach;
							?>
							<th class="not-export-col">Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php
						foreach($data_dosen as $data_key=>$data_val):
							?>
							<tr>
								<?php
								foreach($data_column as $data_column_key=>$data_column_val): ?>
								<td><?=(strlen($data_val->$data_column_val) > 10) ? "<span title='".$data_val->$data_column_val."'>".substr($data_val->$data_column_val,0,10)."...</span>" : $data_val->$data_column_val;?></td>
								<?php
								endforeach;
								?>
								<td>
									<?php
									if($data_val->$kolomID == $this->session->userdata("{$akses}_username")):
									?>
									<a href="<?=base_url($akses.'/'.$pageName.'/edit?'.$kolomID.'='.$data_val->$kolomID);?>"><button type="button" class="btn btn-warning btn-block">Edit</button></a>
									<?php
									endif; ?>
								</td>
							</tr>
						<?php
						endforeach;
						?>
					</tbody>
				</table>
			</div>
			<?php
		break;
		case 'edit': ?>
			<form action="<?=base_url($akses.'/'.$pageName.'/'.$tipe_halaman.'_act')?>" class="form-login col-md-6" id="form-login" enctype="multipart/form-data" method="POST" data-parsley-validate>
				<div class="form-label-group">
					<input type="text" id="inputUsername" name="inputUsername" class="form-control" placeholder="NIP / NIDN" required maxlength="18" minlength="18" pattern="[*\d]{18,18}" data-parsley-type="number" <?=(isset($data_dosen) ? "readonly value='{$data_dosen[0]->$data_column[0]}'" : "" )?>>
					<label for="inputUsername">NIP / NIDN</label>
				</div>
				<div class="form-label-group">
					<input type="password" id="inputPassword1" name="inputPassword[]" class="form-control" placeholder="Password" required>
					<label for="inputPassword1">Password</label>
				</div>
				<div class="form-label-group">
					<input type="text" id="inputName" name="inputName[]" class="form-control" placeholder="Nama" required pattern="^[A-Za-z\s]+$" <?=(isset($data_dosen) ? "value='{$data_dosen[0]->$data_column[2]}'" : "" )?>>
					<label for="inputName">Nama Lengkap</label>
				</div>
				<div class="form-label-group">
					<input type="email" id="inputEmail1" name="inputEmail[]" class="form-control" placeholder="E-mail" required <?=(isset($data_dosen) ? "value='{$data_dosen[0]->$data_column[3]}'" : "" )?>>
					<label for="inputEmail1">E-mail</label>
				</div>
				<div class="form-label-group">
					<input type="text" id="inputNoTelp" name="inputNoTelp" class="form-control" placeholder="Nomor Telepon" required pattern="^(\+62)(\d{5,13})$" minlength="8" maxlength="16" <?=(isset($data_dosen) ? "value='{$data_dosen[0]->$data_column[4]}'" : "" )?>>
					<label for="inputNoTelp">Nomor Telepon (Format +62xxxx)</label>
				</div>
				<div class="btn-group special btn-group-lg text-center" role="group" aria-label="Tambah Action">
					<button class="btn btn-warning" type="reset"><i class="fa fa-undo"></i>Reset</button>
					<button class="btn btn-primary" type="submit"><i class="fa fa-<?=(!isset($data_dosen) ? "plus-square" : "edit" )?>"></i><?=ucwords($tipe_halaman)?>  <?=ucwords($pageName)?></button>
				</div>
			</form>
			<?php
		break;
	endswitch;
} //penutup IF ISSET EXPLODE

$this->load->view("{$akses}/inc/footer");
$this->load->view("inc/footer"); ?>