<?php $this->load->view("inc/header"); ?>
	<link rel="stylesheet" href="<?=base_url()?>assets/css/sign_in.css">
</head>
<body style="background:#a7bcff;">
	<form action="<?=base_url($pageName.'/'.$pageName.'_act/mahasiswa')?>" class="form-login" id="form-login" method="POST" data-parsley-validate  enctype="multipart/form-data">
		<div class="text-center mb-4">
			<img class="mb-4" src="<?=$this->ikonWeb?>" alt="" width="72" height="72">
			<h1 class="h3 mb-3 font-weight-normal"><?=$this->namaWeb?> siap melayani anda</h1>
			<p>Masukkan detail dari profil <b><?=strtoupper($akses)?></b> anda sebagai detail dalam <?=$this->namaWeb?></p>
		</div>
		<?php echo $this->session->flashdata('alert'); ?>
		<div class="form-row">
			<div class="col">
				<div class="form-label-group">
					<input type="text" id="inputNameF" name="inputName[]" class="form-control" placeholder="Nama depan" required autofocus pattern="^[A-Za-z\s]+$">
					<label for="inputNameF">Nama depan</label>
				</div>
			</div>
			<div class="col">
				<div class="form-label-group">
					<input type="text" id="inputNameB" name="inputName[]" class="form-control" placeholder="Nama belakang" required pattern="^[A-Za-z\s]+$">
					<label for="inputNameB">Nama belakang</label>
				</div>
			</div>
		</div>
		<div class="form-label-group">
			<input type="text" id="inputNoTelp" name="inputNoTelp" class="form-control" placeholder="Nomor Telepon" required pattern="^(\+62)(\d{5,13})$" minlength="8" maxlength="16">
			<label for="inputNoTelp">Nomor Telepon (Format +62xxxx)</label>
		</div>
		<div class="form-label-group">
			<input type="email" id="inputEmail1" name="inputEmail[]" class="form-control" placeholder="E-mail" required>
			<label for="inputEmail1">E-mail</label>
		</div>

		<div class="form-label-group">
			<input type="email" id="inputEmail2" name="inputEmail[]" class="form-control" placeholder="Konfirmasi E-mail" required data-parsley-equalto="#inputEmail1">
			<label for="inputEmail2">Konfirmasi E-mail</label>
		</div>

		<div class="form-label-group">
			<input type="text" id="inputUsername" name="inputUsername" class="form-control" placeholder="NIM" required maxlength="12"minlength="12" pattern="[*\d]{12,12}" data-parsley-type="number">
			<label for="inputUsername">NIM</label>
		</div>
		<div class="form-label-group">
			<input type="password" id="inputPassword1" name="inputPassword[]" class="form-control" placeholder="Password" required>
			<label for="inputPassword1">Password</label>
		</div>

		<div class="form-label-group">
			<input type="password" id="inputPassword2" name="inputPassword[]" class="form-control" placeholder="Konfirmasi Password" required data-parsley-equalto="#inputPassword1">
			<label for="inputPassword2">Konfirmasi Password</label>
		</div>
		<div class="custom-control custom-checkbox">
			<input id="persyaratan1" type="checkbox" class="custom-control-input" value="setuju-persyaratan" name="persyaratan[]" required>
			<label class="custom-control-label" for="persyaratan1">Saya menyetujui <a href="<?=base_url('syaratketentuan')?>" target="_blank">SYARAT DAN KETENTUAN</a> <?=$this->namaWeb?>.</label>
		</div>
		<div class="btn-group special btn-group-lg text-center" role="group" aria-label="SignUp Action">
			<button class="btn btn-warning" type="reset">Reset</button>
			<button class="btn btn-primary" type="submit">Sign Up</button>
		</div>
		<p class="text-center">Sudah memiliki akun? Silahkan <a href="<?=base_url('login')?>">Login</a> disini</p>
		<p class="mt-5 mb-3 text-muted text-center">&copy; <?=date('Y')?></p>
	</form>

<?php $this->load->view("inc/footer"); ?>