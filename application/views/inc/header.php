<!doctype html>
<html lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"
		>
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	
		<title><?= (isset($title)) ? $title." - " : "" ; ?><?=$this->namaWeb;?></title>
		<!-- favicon -->
		<link rel="shortcut icon" href="<?=$this->ikonWeb;?>">

		<!-- PLUGIN 
		===============
		-->
		<!-- BS -->
		<link rel="stylesheet" href="<?=base_url()?>assets/plugin/bs-4.0/css/bootstrap.min.css">
		<!-- PARSLEY -->
		<link rel="stylesheet" href="<?=base_url()?>assets/plugin/parsley/parsley.css">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="<?=base_url()?>assets/plugin/fa-5.5.0/css/all.min.css">
		<!-- DataTables -->
		<link rel="stylesheet" href="<?=base_url()?>assets/plugin/DataTables/datatables.css">
		<!-- DataTables -->
		<link rel="stylesheet" href="<?=base_url()?>assets/plugin/Easy-Datetime-Picker-Bootstrap/css/bootstrap-datetimepicker.min.css">
	    <!-- Scrollbar Custom CSS -->
	    <link rel="stylesheet" href="<?=base_url()?>assets/plugin/jquery.mCustomScrollbar.min.css">
	    <!-- Highcharts -->
	    <link rel="stylesheet" href="<?=base_url()?>assets/plugin/Highcharts-6.2.0/code/css/highcharts.css">
	    <!-- Fonts -->
	    <link rel="stylesheet" href="<?=base_url()?>assets/fonts/poppins/poppins.css">


		<!-- JAVASCRIPT 
		===============
		-->
		<script type="text/javascript" src="<?=base_url()?>assets/plugin/jquery-3.3.1.min.js"></script>
		<script type="text/javascript" src="<?=base_url()?>assets/plugin/popper.min.js"></script>
		<script type="text/javascript" src="<?=base_url()?>assets/plugin/jquery.mousewheel.min.js"></script>
		<!-- PLUGIN
		============ -->
		<!-- BS -->
		<script type="text/javascript" src="<?=base_url()?>assets/plugin/bs-4.0/js/bootstrap.min.js"></script>
		<!-- fontawesome -->
		<script type="text/javascript" src="<?=base_url()?>assets/plugin/fa-5.5.0/js/all.min.js"></script>
		<!-- parsley -->
		<script type="text/javascript" src="<?=base_url()?>assets/plugin/parsley/parsley.js"></script>
		<script type="text/javascript" src="<?=base_url()?>assets/plugin/parsley/i18n/id.js"></script>
		<!-- DataTables -->
		<script type="text/javascript" src="<?=base_url()?>assets/plugin/DataTables/datatables.js"></script>
		<!-- DateTime Picker-->
		<script type="text/javascript" src="<?=base_url()?>assets/plugin/Easy-Datetime-Picker-Bootstrap/js/bootstrap-datetimepicker.min.js"></script>
		<script type="text/javascript" src="<?=base_url()?>assets/plugin/Easy-Datetime-Picker-Bootstrap/js/locales/bootstrap-datetimepicker.id.js"></script>
	    <!-- jQuery Custom Scroller CDN -->
	    <script type="text/javascript" src="<?=base_url()?>assets/plugin/jquery.mCustomScrollbar.concat.min.js"></script>
	    <!-- Video -->
	    <script type="text/javascript" src="<?=base_url()?>assets/plugin/Vide/jquery.vide.js"></script>
	    <!-- HighChart -->
	    <script type="text/javascript" src="<?=base_url()?>assets/plugin/Highcharts-6.2.0/code/js/highcharts.js"></script>
	    <script type="text/javascript" src="<?=base_url()?>assets/plugin/Highcharts-6.2.0/code/js/themes/dark-unica.js"></script>
		<script type="text/javascript" src="<?=base_url()?>assets/plugin/Highcharts-6.2.0/code/modules/exporting.js"></script>
		<script type="text/javascript" src="<?=base_url()?>assets/plugin/Highcharts-6.2.0/code/modules/export-data.js"></script>


		<!-- Core CSS -->
		<link rel="stylesheet" href="<?=base_url()?>assets/css/core.css">
		<!-- CORE JS -->
		<script type="text/javascript" src="<?=base_url()?>assets/js/core.js"></script>