<nav id="sidebar">
	<div id="dismiss">
		<i class="fas fa-arrow-left"></i>
	</div>

	<div class="sidebar-header">
		<h3>MENU <?=strtoupper("{$akses}");?></h3>
	</div>
	<?php
	if($this->input->cookie("{$akses}_remember_token", TRUE) == TRUE){
		$displayName = $this->input->cookie("{$akses}_dt1", TRUE);
	}else if($this->session->userdata("{$akses}_logged_in")){
		$displayName = $this->session->userdata("{$akses}_username");
	}
	?>
	<ul class="sidebar list-unstyled components">
		<p>Selamat datang, <?="{$displayName}"?></p>
		<?php		
		$i = 0;
		foreach($link_list as $slugLink => $titleLink){ 
			$current = ($slugLink==$slugLoaded) ? "current-url" : "" ;	
		?>
		<li>
			<?php if(is_array($titleLink) || $titleLink instanceof Countable):
				$posisi = strpos($slugLoaded,strtolower($titleLink['title']));
				$current = ($posisi!==false && $posisi==0) ? "current-url" : "" ;
				?>
				<a href="#subMenu<?=$i;?>" data-toggle="collapse" aria-expanded="false"class='<?=$current?>'><?=$titleLink['title']?></a>
				<ul class="collapse list-unstyled" id="subMenu<?=$i;?>">
				<?php
				foreach($titleLink as $linkSub=>$linkSubTitle):
					$current = ($linkSub==$slugLoaded) ? "current-url" : "" ;
					if($linkSub!= 'title'):
					?>
						<li>
							<a href="<?=base_url($akses.'/'.$linkSub);?>" class='<?=$current?>'>
								<?=$linkSubTitle;?>
							</a>
						</li>
					<?php
					endif;
				endforeach; 
				$i++;?>        	
				</ul>
			<?php else: ?>
				<a href="<?=base_url($akses.'/'.$slugLink);?>" class='<?=$current?>'><?=$titleLink;?></a>			
			<?php endif;?>
		</li>
		<?php
		}
		?>
	</ul>
</nav>
