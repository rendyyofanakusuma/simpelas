<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['login/(:any)']  = 'login/index/$1';
$route['signup/(:any)']  = 'signup/index/$1';

$route['mahasiswa/(:any)/(:any)']  = 'mahasiswa/$1/index/$2';
$route['dosen/(:any)/(:any)']  = 'dosen/$1/index/$2';
$route['admin/(:any)/(:any)']  = 'admin/$1/index/$2';
$route['translate_uri_dashes'] = FALSE;
