<?php
defined("BASEPATH") OR exit("No direct script access allowed");
class Signup extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index($akses = "mahasiswa"){
		$pageName = "signup";
		$akses = strtolower($akses);

		if($this->session->userdata("{$akses}_logged_in") OR $this->input->cookie("{$akses}_remember_token"))
			redirect("{$akses}/home/");
		if($akses!="mahasiswa")
			redirect("{$akses}/login/");
		$slugLoaded = $pageName;
		$slugLoaded .= ($akses!='') ? "/$akses" : "" ;
		if ( ! file_exists(APPPATH."views/{$pageName}.php"))
			show_404();
		else if(!file_exists(APPPATH."views/{$akses}")){
			alert_danger("Akses Gagal", "Hak akses <b>".ucwords($akses)."</b> tidak ditemukan");
			redirect("$page");				
		}
		$data = array(
			"title"			=> ucwords("{$pageName} {$akses}"),
			"pageName"		=> $pageName,
			"slugLoaded"	=> $slugLoaded,
			"akses"			=> $akses,
		);
		$this->load->view($pageName, $data);
	}


	public function signup_act($akses="mahasiswa"){
		$params		= $this->input->post(null, true);
		$nama 	   	= implode(" ",$params['inputName']);
		$username  	= $params['inputUsername'];
		$password 	= $params['inputPassword'];
		$noTelp	   	= $params['inputNoTelp'];
		$email 	   	= $params['inputEmail'];
		$data_insert = array("{$akses}_email" 	=> $email[1]);
		if($this->model_data->fetch_default("{$akses}_login", $data_insert)){
			alert_danger("Gagal Signup", "E-mail telah terdaftar");
			redirect("signup/{$akses}");
		}
		$data_insert["{$akses}_username"] = $username;
		if($this->model_data->fetch_default("{$akses}_login", $data_insert)){
			alert_danger("Gagal Signup", "Username telah terdaftar");
			redirect("login/{$akses}");
		}else{
			$data_insert["{$akses}_password"] 	= md5($password[1]);
			$data_insert["{$akses}_nama"] 		= $nama;
			$data_insert["{$akses}_notelp"] 	= $noTelp;
			if($this->model_akun->do_signup($data_insert)){
				alert_success("Sukses terdaftar", "Silahkan login sebagai {$akses}");
				redirect("login/{$akses}");
			}else{
				alert_danger("Gagal Signup", "Terjadi kesalahan, silahkan ulangi lagi");
				redirect("signup/{$akses}");
			}
		}
	}

}