<?php
defined("BASEPATH") OR exit("No direct script access allowed");
class Matakuliah extends CI_Controller {

	public function __construct(){
		parent::__construct();


		//gVar = Global Variabel
		$akses = strtolower($this->uri->segment(1));
		$pageName = $this->router->fetch_class();

		$this->gVar = array(
			'akses' 	=> $akses,
			'pageName'	=> $this->router->fetch_class(),
			'linkList' 	=> $this->model_link_list->link_list($akses),
			'kolomID' 	=> $this->model_data->fetch_column_PK($pageName),
		);
		$this->model_security->logged_in($akses);
	}

	public function index($pc1 = "daftar"){
		$gVar = $this->gVar;
		$slugLoaded = $gVar['pageName'];
		$slugLoaded .= ($pc1!="") ? "/{$pc1}" : "" ;		
		if ( ! file_exists(APPPATH."views/{$gVar['akses']}/{$gVar['pageName']}.php")){
			redirect("home");
		}
		$data = array(
			"title"			=> ucwords($pc1)." ".strtoupper($gVar['pageName']),
			"slugLoaded"	=> $slugLoaded,
			"pageName"		=> $gVar['pageName'],
			"akses"			=> $gVar['akses'],
			"link_list"		=> $gVar['linkList'],
			"kolomID"		=> $gVar['kolomID'],
		);

		switch($pc1):
			case 'daftar':
				$data['data_column'] = $this->model_data->fetch_column_name($gVar['pageName']);
				$where = array(
					"{$gVar['akses']}_username" => $this->session->userdata("{$gVar['akses']}_username"),
				);
				$inMatkul = array();
				$data['data_ambil_matkul'] = array();
				$data['data_matkul'] = array();
				$data['data_ambil_matkul'] = $this->model_data->fetch_default("ambil_matakuliah", $where);
				if($data['data_ambil_matkul']):
					foreach($data['data_ambil_matkul'] as $key=>$val):
						$inMatkul[] = $val->id_matkul;
					endforeach;
					$data['data_matkul'] = $this->model_data->fetch_default("matakuliah",$inMatkul, "=", "*", "", "", "id_matkul");
				endif;
			break;
		endswitch;
		
		$this->load->view("{$gVar['akses']}/{$gVar['pageName']}", $data);
	}
}