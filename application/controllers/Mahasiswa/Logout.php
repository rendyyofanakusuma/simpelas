<?php
defined("BASEPATH") OR exit("No direct script access allowed");
class Logout extends CI_Controller {

	public function __construct(){
		parent::__construct();


		//gVar = Global Variabel
		$akses = strtolower($this->uri->segment(1));

		$this->gVar = array(
			'akses' 	=> $akses,
		);
		$this->model_security->logged_in($akses);
	}

	public function index(){
		$gVar = $this->gVar;
		alert_warning("Berhasil Logout", "Apabila ingin mengakses kembali, silahkan login.");
		$this->model_security->batch_delete_cookies($gVar['akses']);
		$this->session->unset_userdata("{$gVar['akses']}_logged_in");
		$this->session->unset_userdata("{$gVar['akses']}_username");
		$this->session->unset_userdata("{$gVar['akses']}_password");

		redirect("login/{$gVar['akses']}");
	}

}