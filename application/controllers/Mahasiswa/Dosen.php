<?php
defined("BASEPATH") OR exit("No direct script access allowed");
class Dosen extends CI_Controller {

	public function __construct(){
		parent::__construct();


		//gVar = Global Variabel
		$akses = strtolower($this->uri->segment(1));
		$pageName = $this->router->fetch_class();

		$this->gVar = array(
			'akses' 	=> $akses,
			'pageName'	=> $this->router->fetch_class(),
			'linkList' 	=> $this->model_link_list->link_list($akses),
			'kolomID' 	=> $this->model_data->fetch_column_PK("{$pageName}_login"),
		);
		$this->model_security->logged_in($akses);
	}

	public function index($pc1 = "daftar", $pc2 = ""){
		$gVar = $this->gVar;
		$slugLoaded = $gVar['pageName'];
		$slugLoaded .= ($pc1!="") ? "/{$pc1}" : "" ;
		
		if ( ! file_exists(APPPATH."views/{$gVar['akses']}/{$gVar['pageName']}.php")){
			redirect("home");
		}

		$data = array(
			"title"			=> ucwords($pc1)." ".strtoupper($gVar['pageName']),
			"pageName"		=> $gVar['pageName'],
			"slugLoaded"	=> $slugLoaded,
			"akses"			=> $gVar['akses'],
			"link_list"		=> $gVar['linkList'],
			"kolomID"		=> $gVar['kolomID'],
		);

		switch($pc1):
			case 'daftar':
				$data["data_column"] = $this->model_data->fetch_column_name($gVar['pageName']."_login");
				$data["data_{$gVar['pageName']}"] = $this->model_data->fetch_default($gVar['pageName']."_login");
			break;
			case 'edit':
				if(!$this->input->get("{$gVar['kolomID']}"))
					redirect(base_url("{$gVar['akses']}/{$gVar['pageName']}/daftar"));
				$data['data_column'] = $this->model_data->fetch_column_name("{$gVar['pageName']}_login");
				$query = $this->input->get("{$gVar['kolomID']}");
				$where = array(
					"{$gVar['kolomID']}" => $query,
				);
				$data["data_{$gVar['pageName']}"] = $this->model_data->fetch_default("{$gVar['pageName']}_login",$where);
				if($data["data_{$gVar['pageName']}"]==null)
					redirect(base_url("{$gVar['akses']}/{$gVar['pageName']}/daftar"));

			break;
			case 'hapus':
				if(!$this->input->get("{$gVar['kolomID']}"))
					redirect(base_url("{$gVar['akses']}/{$gVar['pageName']}/daftar"));
				$query = $this->input->get("{$gVar['kolomID']}");
				$where = array(
					"{$gVar['kolomID']}" => $query,
				);
				$act = $this->model_data->delete_default("{$gVar['pageName']}_login", $where);
				if($act){
					alert_success("Sukses Dihapus", "Username {$query} telah dihapus");
				}else{
					alert_success("Gagal Dihapus", "Username {$query} gagal dihapus");					
				}
				redirect(base_url("{$gVar['akses']}/{$gVar['pageName']}/daftar"));
			break;

			/*
			===========
			AKSI
			===========
			*/
			case 'tambah_act':
				$params		= $this->input->post(null, true);
				$nama 	   	= implode(" ",$params['inputName']);
				$username  	= $params['inputUsername'];
				$password 	= $params['inputPassword'];
				$noTelp	   	= $params['inputNoTelp'];
				$email 	   	= $params['inputEmail'];

				if(!isset($params)){
					redirect(base_url("{$gVar['akses']}/{$gVar['pageName']}/tambah"));
				}
				$data_insert = array("{$gVar['pageName']}_email" 	=> $email[0]);
				if($this->model_data->fetch_default("{$gVar['pageName']}_login", $data_insert)){
					alert_danger("Gagal Signup", "E-mail telah terdaftar");
					redirect(base_url("{$gVar['akses']}/{$gVar['pageName']}/daftar"));
				}
				$data_insert["{$gVar['kolomID']}"] = $username;
				if($this->model_data->fetch_default("{$gVar['pageName']}_login", $data_insert)){
					alert_danger("Gagal Signup", "Username telah terdaftar");
					redirect(base_url("{$gVar['akses']}/{$gVar['pageName']}/daftar"));
				}else{
					$data_insert["{$gVar['pageName']}_password"] 	= md5($password[0]);
					$data_insert["{$gVar['pageName']}_nama"] 		= $nama;
					$data_insert["{$gVar['pageName']}_notelp"] 	= $noTelp;
					if($this->model_data->insert_default("{$gVar['pageName']}_login",$data_insert)){
						alert_success("Sukses terdaftar", "{$gVar['pageName']} sukses terdaftar");
					}else{
						alert_danger("Gagal Signup", "Terjadi kesalahan, silahkan ulangi lagi");
					}
					redirect(base_url("{$gVar['akses']}/{$gVar['pageName']}/daftar"));
				}
				redirect(base_url("{$gVar['akses']}/{$gVar['pageName']}/daftar"));
			break;
			case 'edit_act':
				$params		= $this->input->post(null, true);
				$username  	= $params['inputUsername'];
				$password 	= $params['inputPassword'];
				$nama 	   	= $params['inputName'];
				$noTelp	   	= $params['inputNoTelp'];
				$email 	   	= $params['inputEmail'];
				$where = array(
					"{$gVar['kolomID']}" => $username,
				);
				if(!isset($params))
					redirect(base_url("{$gVar['akses']}/{$gVar['pageName']}/daftar"));
				$cek = array(
					"{$gVar['pageName']}_email" 	=> $email[0],
					"{$gVar['kolomID']}" => "!={$username}",

				);
				if($this->model_data->fetch_default("{$gVar['pageName']}_login", $cek)){
					alert_danger("Gagal Ubah", "E-mail telah terdaftar");
					redirect(base_url("{$gVar['akses']}/{$gVar['pageName']}/daftar"));
				}
				$set = array(
					"{$gVar['pageName']}_email" 	=> $email[0],
					"{$gVar['pageName']}_password"	=> md5($password[0]),
					"{$gVar['pageName']}_nama"		=> $nama[0],
					"{$gVar['pageName']}_notelp" 	=> $noTelp,
				);
				$act = $this->model_data->update_default("{$gVar['pageName']}_login", $set, $where);
				if($act){
					alert_success("Sukses diubah", "{$gVar['pageName']} dengan id {$username} sukses diubah");
				}else{
					alert_danger("Gagal ubah", "Terjadi kesalahan, silahkan ulangi lagi");
				}
				redirect(base_url("{$gVar['akses']}/{$gVar['pageName']}/daftar"));
			break;
		endswitch;
		$this->load->view("{$gVar['akses']}/{$gVar['pageName']}", $data);
	}
}