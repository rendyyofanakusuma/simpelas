<?php
defined("BASEPATH") OR exit("No direct script access allowed");
class Tugas extends CI_Controller {

	public function __construct(){
		parent::__construct();


		//gVar = Global Variabel
		$akses = strtolower($this->uri->segment(1));
		$pageName = $this->router->fetch_class();

		$this->gVar = array(
			'akses' 	=> $akses,
			'pageName'	=> $this->router->fetch_class(),
			'linkList' 	=> $this->model_link_list->link_list($akses),
			'kolomID' 	=> $this->model_data->fetch_column_PK($pageName),
			'FFile' 	=> array("Gambar"=>"jpg|jpeg|png", "Dokumen PDF" => "pdf", "Dokumen Word (cth : doc,docx)" => "doc|docx", "Dokumen Excel (cth : xls,xlsx)" => "xls|xlsx", "File Archive (cth : rar,zip)" => "rar|zip|7z"),
			'FNama' 	=> array("Nama Mahasiswa" => "mahasiswa_nama", "NIM"=>"mahasiswa_username", "Nama Tugas" => "nama_tugas", ),
		);
		$this->model_security->logged_in($akses);
	}

	public function index($pc1 = "daftar"){
		$gVar = $this->gVar;
		$slugLoaded = $gVar['pageName'];
		$slugLoaded .= ($pc1!="") ? "/{$pc1}" : "" ;		
		if ( ! file_exists(APPPATH."views/{$gVar['akses']}/{$gVar['pageName']}.php")){
			redirect("home");
		}
		$data = array(
			"title"			=> ucwords($pc1)." ".strtoupper($gVar['pageName']),
			"slugLoaded"	=> $slugLoaded,
			"pageName"		=> $gVar['pageName'],
			"akses"			=> $gVar['akses'],
			"link_list"		=> $gVar['linkList'],
			"kolomID"		=> $gVar['kolomID'],
		);
		switch($pc1):
			case 'daftar':
				$data['data_column'] = $this->model_data->fetch_column_name($gVar['pageName']);
				$where = "";
				$data['data_tugas'] = array();
				if($this->input->get("id_matkul")){
					$where = array(
						"id_matkul" => $this->input->get("id_matkul"),
					);
					$data['data_tugas'] = $this->model_data->fetch_default($gVar['pageName'],$where);
				}else{
					$where = array(
						"{$gVar['akses']}_username" => $this->session->userdata("{$gVar['akses']}_username"),
					);
					$inMatkul = array();
					$data['data_ambil_matkul'] = $this->model_data->fetch_default("ambil_matakuliah",$where);
					if($data['data_ambil_matkul']):
						foreach($data['data_ambil_matkul'] as $key=>$val):
							$inMatkul[] = $val->id_matkul;
						endforeach;
						$data['data_tugas'] = $this->model_data->fetch_default("tugas",$inMatkul, "=", "*", "", "", "id_matkul");
					endif;
				}
				// CEK TUGAS YANG SUDAH DIKUMPULKAN
				$data["data_tugas_kumpul"] = $this->model_data->fetch_default("tugas_kumpul");
			break;
			case 'upload':
				if(!$this->input->get("{$gVar['kolomID']}"))
					redirect(base_url("{$gVar['akses']}/{$gVar['pageName']}/daftar"));
				$query = $this->input->get("{$gVar['kolomID']}");
				$where = array(
					"{$gVar['kolomID']}" => $query,
				);
				$data['data_tugas'] = $this->model_data->fetch_default($gVar['pageName'],$where);
				$data['data_column'] = $this->model_data->fetch_column_name($gVar['pageName']);
				if($data['data_tugas']==null)
					redirect(base_url("{$gVar['akses']}/{$gVar['pageName']}/daftar"));
			break;

			/*
			===========
			AKSI
			===========
			*/
			case "upload_act":
				$params = $this->input->post(null, true);
				if(!isset($params) || !isset($params["{$gVar['kolomID']}"]))
					redirect(base_url("{$gVar['akses']}/{$gVar['pageName']}/daftar"));
				//Fetch Data
				$where = array(
					"{$gVar['kolomID']}" => $params["{$gVar['kolomID']}"],
				);
				$data["data_{$gVar['pageName']}"] = $this->model_data->fetch_default("{$gVar['pageName']}",$where);

				// Cek apakah ada format penamaan
				if($data["data_{$gVar['pageName']}"][0]->format_nama != ""):
					$delimiter = $data["data_{$gVar['pageName']}"][0]->format_nama_delimiter;
					$formatNama = $data["data_{$gVar['pageName']}"][0]->format_nama;
					$NIM = $this->session->userdata("{$gVar['akses']}_username");
					//Fetch Data Siswa
					$where = array(
						"mahasiswa_username" => $NIM,
					);
					$data["data_mahasiswa_login"] = $this->model_data->fetch_default("mahasiswa_login",$where);

					$config['file_name'] = "";
					$namaArray = array();
					//Set Nama File
					foreach(explode($delimiter, $formatNama) as $val):
						//remove space
						//remove name that include delimiter
						$removedChar = array(" ", $delimiter);
						if(strpos($val,"tugas")!==false){
							$namaArray[] = str_replace($removedChar,"",$data["data_tugas"][0]->nama_tugas);
						}else if(strpos($val,"mahasiswa")!==false){
							$namaArray[] = str_replace($removedChar,"",$data["data_mahasiswa_login"][0]->$val);
						}
					endforeach;
					$config['file_name'] = implode($delimiter, $namaArray);
				endif;


				$config['upload_path']          = "./uploads/tugas/".$params["{$gVar['kolomID']}"];
				$config['allowed_types']        = $data["data_{$gVar['pageName']}"][0]->format_file;

				//buat direktori

				if (!is_dir('uploads/tugas'))
				{
					mkdir('./uploads/tugas', 0777, true);
				}
				$dir_exist = true; // flag for checking the directory exist or not
				if (!is_dir('uploads/tugas/' . $params["{$gVar['kolomID']}"]))
				{
					mkdir('./uploads/tugas/' . $params["{$gVar['kolomID']}"], 0777, true);
					$dir_exist = false; // dir not exist
				}


				//RUN UPLOAD
				$this->upload->initialize($config);

				// //Cek jika file ada
				$filename= $_FILES["file_tugas_upload"]["name"];
				$file_ext = pathinfo($filename,PATHINFO_EXTENSION);
				$fileChecked = glob ("./uploads/tugas/".$params["{$gVar['kolomID']}"]."/{$config['file_name']}.*");
				//Jika file ada, langsung delete
				if($fileChecked){
					foreach ($fileChecked as $file) {
						unlink($file);
						echo $file;
					}
				}

				if (!$this->upload->do_upload('file_tugas_upload')){
					if(!$dir_exist)
						rmdir('./uploads/tugas/' . $params["{$gVar['kolomID']}"]);
					alert_danger(implode("<br>",$config), $this->upload->display_errors('<span>', '</span>'));
				}
				else{
					$data_insert=array(
						"id_tugas"				=> $params["{$gVar['kolomID']}"],
						"mahasiswa_username" 	=> "{$NIM}",
					);
					//masuk database
					$check = $this->model_data->fetch_default("{$gVar['pageName']}_kumpul", $data_insert);
					if(is_array($check) || $check instanceof Countable){
						//delete all first if sebelumnya udah upload
						$act = $this->model_data->delete_default("{$gVar['pageName']}_kumpul", $data_insert);
					}
					$data_insert["nama_file"] = "{$config['file_name']}.{$file_ext}";
					$act = $this->model_data->insert_default("{$gVar['pageName']}_kumpul", $data_insert);
					if($act){
						alert_success("Sukses", "Anda telah berhasil upload dengan nama '{$data_insert['nama_file']}'");
					}
				}
			redirect(base_url("{$gVar['akses']}/{$gVar['pageName']}/daftar"));
			break;
		endswitch;

		$this->load->view("{$gVar['akses']}/{$gVar['pageName']}", $data);
	}
}