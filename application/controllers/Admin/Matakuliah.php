<?php
defined("BASEPATH") OR exit("No direct script access allowed");
class Matakuliah extends CI_Controller {

	public function __construct(){
		parent::__construct();


		//gVar = Global Variabel
		$akses = strtolower($this->uri->segment(1));
		$pageName = $this->router->fetch_class();

		$this->gVar = array(
			'akses' 	=> $akses,
			'pageName'	=> $this->router->fetch_class(),
			'linkList' 	=> $this->model_link_list->link_list($akses),
			'kolomID' 	=> $this->model_data->fetch_column_PK($pageName),
		);
		$this->model_security->logged_in($akses);
	}

	public function index($pc1 = "daftar"){
		$gVar = $this->gVar;
		$slugLoaded = $gVar['pageName'];
		$slugLoaded .= ($pc1!="") ? "/{$pc1}" : "" ;		
		if ( ! file_exists(APPPATH."views/{$gVar['akses']}/{$gVar['pageName']}.php")){
			redirect("home");
		}
		$data = array(
			"title"			=> ucwords($pc1)." ".strtoupper($gVar['pageName']),
			"slugLoaded"	=> $slugLoaded,
			"pageName"		=> $gVar['pageName'],
			"akses"			=> $gVar['akses'],
			"link_list"		=> $gVar['linkList'],
			"kolomID"		=> $gVar['kolomID'],
		);

		switch($pc1):
			case 'daftar':
				$data['data_column'] = $this->model_data->fetch_column_name($gVar['pageName']);
				$data['data_matkul'] = $this->model_data->fetch_default($gVar['pageName']);
			break;				
			case 'tambah':		
			case 'edit':
				$data['data_dosen'] = $this->model_data->fetch_default("dosen_login");
				if(isset($data['data_dosen']) AND $data['data_dosen']!=null):
					switch($pc1){
						case 'edit':
							if(!$this->input->get("{$gVar['kolomID']}"))
								redirect(base_url("{$gVar['akses']}/{$gVar['pageName']}/daftar"));
							$data['data_column'] = $this->model_data->fetch_column_name($gVar['pageName']);
							$query = $this->input->get("{$gVar['kolomID']}");
							$where = array(
								"{$gVar['kolomID']}" => $query,
							);
							$data['data_ambil_mk'] = $this->model_data->fetch_default("ambil_matakuliah", $where);
							$data['data_matkul'] = $this->model_data->fetch_default($gVar['pageName'],$where);
						break;
					}
				else:
					alert_danger("Data kosong", "Harap isi dosen terlebih dahulu");
					redirect(base_url("{$gVar['akses']}/dosen/tambah"));
				endif;
			break;
			case 'hapus':
				if(!$this->input->get("{$gVar['kolomID']}"))
					redirect(base_url("{$gVar['akses']}/{$gVar['pageName']}/daftar"));
				$query = $this->input->get("{$gVar['kolomID']}");
				$where = array(
					"{$gVar['kolomID']}" => $query,
				);
				$act = $this->model_data->delete_default("{$gVar['pageName']}", $where);
				if($act){
					alert_success("Sukses Dihapus", "{$gVar['pageName']} {$query} telah dihapus");
				}else{
					alert_danger("Gagal Dihapus", "{$gVar['pageName']} {$query} gagal dihapus");		
				}
				redirect(base_url("{$gVar['akses']}/{$gVar['pageName']}/daftar"));
			break;
			case 'mahasiswa':
				$data['data_matkul'] = $this->model_data->fetch_default($gVar['pageName']);
				if(isset($data['data_matkul']) AND $data['data_matkul']!=null):
					$id_matkul_current = $this->input->get("{$gVar['kolomID']}");
					if(!$id_matkul_current OR $id_matkul_current==NULL)
						$id_matkul_current = $data['data_matkul'][0]->id_matkul;

					$joinArray = array(
						array('mahasiswa_login', 'ambil_matakuliah.mahasiswa_username = mahasiswa_login.mahasiswa_username', 'right'),
						array('matakuliah', 'ambil_matakuliah.id_matkul = matakuliah.id_matkul', 'left'),
					);
					$where = array(
						"matakuliah.id_matkul" => $id_matkul_current,
					);
					$data['id_matkul_current'] = $id_matkul_current;
					$data['data_column'] = array("mahasiswa_username","mahasiswa_nama");
					$data['data_mahasiswa'] = $this->model_data->fetch_default("mahasiswa_login");
					$data_ambil_mk = $this->model_data->fetch_default("ambil_matakuliah", $where, '=','',$joinArray);
					$data_ambil_mk = json_decode(json_encode($data_ambil_mk), true);
					$data_ambil_mk = array_column($data_ambil_mk, 'mahasiswa_username');
					$data['data_ambil_mk'] = $data_ambil_mk;
				else:
					alert_danger("Data kosong", "Harap isi matakuliah terlebih dahulu");
					redirect(base_url("{$gVar['akses']}/matakuliah/tambah"));					
				endif;
			break;

			/*
			===========
			AKSI
			===========
			*/
			case 'tambah_act':
				$params		= $this->input->post(null, true);
				$dosen_username = $params['inputDosen'];
				$nama_matkul = $params['inputNamaMatkul'];

				if(!isset($params)){
					redirect(base_url("{$gVar['akses']}/{$gVar['pageName']}/tambah"));
				}
				$cek = array("nama_matkul" 	=> $nama_matkul);
				if($this->model_data->fetch_default("{$gVar['pageName']}", $cek)){
					alert_danger("Gagal Tambah", "Nama Matakuliah telah terdaftar");
					redirect(base_url("{$gVar['akses']}/{$gVar['pageName']}/daftar"));
				}else{
					$data_insert=array(
						"dosen_username" 	=> $dosen_username,
						"nama_matkul" 	=> $nama_matkul,
					);
					$act = $this->model_data->insert_default("matakuliah",$data_insert);
					if($act){
						alert_success("Sukses terdaftar", "{$gVar['pageName']} sukses terdaftar");
					}else{
						alert_danger("Gagal Signup", "Terjadi kesalahan, silahkan ulangi lagi");
					}
				}
				redirect(base_url("{$gVar['akses']}/{$gVar['pageName']}/daftar"));
			break;
			case 'edit_act':
				$params		= $this->input->post(null, true);
				$dosen_username = $params['inputDosen'];
				$nama_matkul = $params['inputNamaMatkul'];
				$where = array(
					"{$gVar['kolomID']}" => $params["{$gVar['kolomID']}"],
				);
				if(!isset($params) || !isset($params["{$gVar['kolomID']}"]))
					redirect(base_url("{$gVar['akses']}/{$gVar['pageName']}/daftar"));

				$cek = array(
					"nama_matkul" 	=> $nama_matkul,
					"{$gVar['kolomID']}" => "!=".$params["{$gVar['kolomID']}"],);
				if($this->model_data->fetch_default("{$gVar['pageName']}", $cek)){
					alert_danger("Gagal Tambah", "Nama Matakuliah telah terdaftar");
					redirect(base_url("{$gVar['akses']}/{$gVar['pageName']}/daftar"));
				}else{
					$set=array(
						"dosen_username" 	=> $dosen_username,
						"nama_matkul" 	=> $nama_matkul,
					);
					$act = $this->model_data->update_default("{$gVar['pageName']}", $set, $where);
					if($act){
						alert_success("Sukses edit", "Matakuliah dengan id ".$params["{$gVar['kolomID']}"]." telah diedit");
					}else{
						alert_danger("Gagal edit", "Terjadi kesalahan, silahkan ulangi lagi");
					}
				}
				redirect(base_url("{$gVar['akses']}/{$gVar['pageName']}/daftar"));
			break;
			case 'mahasiswa_act':
				$params		= $this->input->post(null, true);
				echo "<br>";
				$id_matkul = $params['id_matkul'];
				$ambil_mk = $params['ambil_mk'];
				$where = array(
					"id_matkul" => $id_matkul,
				);
				$actDeleteAll = $this->model_data->delete_default("ambil_matakuliah", $where);
				if($ambil_mk!=null):
					foreach($ambil_mk as $mahasiswa_username=>$val):
						$data_insert[] = array(
							"id_matkul" => $id_matkul,
							"mahasiswa_username" => $mahasiswa_username,
						);
					endforeach;
					$act = $this->model_data->insert_default("ambil_matakuliah", $data_insert, "batch");
				endif;
				if($act || $actDeleteAll){
					alert_success("Mahasiswa sukses didaftarkan", "Mata kuliah {$id_matkul} berhasil dimodifikasi!");
				}else{
					alert_danger("Mahasiswa GAGAL didaftarkan", "Terjadi kesalahan, silahkan ulangi lagi");
				}


				redirect(base_url("{$gVar['akses']}/{$gVar['pageName']}/mahasiswa?id_matkul={$id_matkul}"));
				
			break;
		endswitch;
		
		$this->load->view("{$gVar['akses']}/{$gVar['pageName']}", $data);
	}
}