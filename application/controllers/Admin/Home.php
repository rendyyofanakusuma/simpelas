<?php
defined("BASEPATH") OR exit("No direct script access allowed");
class Home extends CI_Controller {

	public function __construct(){
		parent::__construct();

		//gVar = Global Variabel
		$akses = strtolower($this->uri->segment(1));
		$pageName = $this->router->fetch_class();

		$this->gVar = array(
			'akses' 	=> $akses,
			'pageName'	=> $this->router->fetch_class(),
			'linkList' 	=> $this->model_link_list->link_list($akses),
			// 'kolomID' 	=> $this->model_data->fetch_column_PK($pageName),
		);
		$this->model_security->logged_in($akses);
	}

	public function index(){
		$gVar = $this->gVar;
		$slugLoaded = $gVar['pageName'];
		
		if ( ! file_exists(APPPATH."views/{$gVar['akses']}/{$gVar['pageName']}.php")){
			redirect("home");
		}
		$data = array(
			"title"			=> ucwords($gVar['pageName'])." ".strtoupper($gVar['akses']),
			"pageName"		=> $gVar['pageName'],
			"slugLoaded"	=> $slugLoaded,
			"akses"			=> $gVar['akses'],
			"link_list"		=> $gVar['linkList'],
			"data_mahasiswa" => $this->model_data->fetch_default("mahasiswa_login"),
			"data_dosen" => $this->model_data->fetch_default("dosen_login"),
			"data_matkul" => $this->model_data->fetch_default("matakuliah"),
		);
		$this->load->view("{$gVar['akses']}/{$gVar['pageName']}", $data);
	}

}