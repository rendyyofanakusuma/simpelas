<?php
defined("BASEPATH") OR exit("No direct script access allowed");
class Shutdown extends CI_Controller {

	public function __construct(){
		parent::__construct();

		//gVar = Global Variabel
		$akses = strtolower($this->uri->segment(1));
		$pageName = $this->router->fetch_class();

		$this->gVar = array(
			'akses' 	=> $akses,
			'pageName'	=> $this->router->fetch_class(),
			'linkList' 	=> $this->model_link_list->link_list($akses),
			'konfirmasiText' => "Saya yakin ingin menghapus semua",
			// 'kolomID' 	=> $this->model_data->fetch_column_PK($pageName),
		);
		$this->model_security->logged_in($akses);
	}

	public function index($pc1 = ""){
		$gVar = $this->gVar;
		$slugLoaded = $gVar['pageName'];
		
		if ( ! file_exists(APPPATH."views/{$gVar['akses']}/{$gVar['pageName']}.php")){
			redirect("home");
		}
		$data = array(
			"title"				=> ucwords($gVar['pageName'])." ".strtoupper($gVar['akses']),
			"pageName"			=> $gVar['pageName'],
			"slugLoaded"		=> $slugLoaded,
			"akses"				=> $gVar['akses'],
			"link_list"			=> $gVar['linkList'],
			"konfirmasiText"	=> $gVar['konfirmasiText'],
		);

		switch($pc1):
			case "shutdown_act":
				$params		= $this->input->post(null, true);
				$konfirmasi  	= $params['inputKonfirmasi'];

				if($konfirmasi === $gVar['konfirmasiText']){
					$act = $this->model_data->shutdown();
					if($act){
						$folderUpDir = "./uploads/tugas/";
						if($this->delete_files($folderUpDir,TRUE, 2)){
							alert_success("Sukses Shutdown", "Semua data sukses dihapus");
						}
					}else{
						alert_success("Gagal Shutdown", "Semua data tidak dihapus");
					}
					redirect(base_url("{$gVar['akses']}/home"));
				}else{
					alert_danger("Gagal Shutdown", "Belum sukses terhapus semua");
					redirect(base_url("{$gVar['akses']}/shutdown/"));
				}
			break;
		endswitch;
		$this->load->view("{$gVar['akses']}/{$gVar['pageName']}", $data);
	}


	function delete_files($path, $del_dir = FALSE, $level = 0)
	{    
		// Trim the trailing slash
		$path = preg_replace("|^(.+?)/*$|", "\\1", $path);
		
		if ( ! $current_dir = @opendir($path))
			return;
	
		while(FALSE !== ($filename = @readdir($current_dir)))
		{
			if ($filename != "." and $filename != "..")
			{
				if (is_dir($path.'/'.$filename))
				{
					// Ignore empty folders
					if (substr($filename, 0, 1) != '.')
					{
						$this->delete_files($path.'/'.$filename, $del_dir, $level + 1);
					}
				}
				else
				{
					unlink($path.'/'.$filename);
				}
			}
		}
		@closedir($current_dir);
	
		if ($del_dir == TRUE AND $level > 0)
		{
			@rmdir($path);
		}
	}

}