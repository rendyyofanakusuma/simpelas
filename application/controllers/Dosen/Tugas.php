<?php
defined("BASEPATH") OR exit("No direct script access allowed");
class Tugas extends CI_Controller {

	public function __construct(){
		parent::__construct();


		//gVar = Global Variabel
		$akses = strtolower($this->uri->segment(1));
		$pageName = $this->router->fetch_class();

		$this->gVar = array(
			'akses' 	=> $akses,
			'pageName'	=> $this->router->fetch_class(),
			'linkList' 	=> $this->model_link_list->link_list($akses),
			'kolomID' 	=> $this->model_data->fetch_column_PK($pageName),
			'FFile' 	=> array("Gambar"=>"jpg|jpeg|png", "Dokumen PDF" => "pdf", "Dokumen Word (cth : doc,docx)" => "doc|docx", "Dokumen Excel (cth : xls,xlsx)" => "xls|xlsx", "File Archive (cth : rar,zip)" => "rar|zip|7z"),
			'FNama' 	=> array("Nama Mahasiswa" => "mahasiswa_nama", "NIM"=>"mahasiswa_username", "Nama Tugas" => "nama_tugas", ),
		);
		$this->model_security->logged_in($akses);
	}

	public function index($pc1 = "daftar"){
		$gVar = $this->gVar;
		$slugLoaded = $gVar['pageName'];
		$slugLoaded .= ($pc1!="") ? "/{$pc1}" : "" ;		
		if ( ! file_exists(APPPATH."views/{$gVar['akses']}/{$gVar['pageName']}.php")){
			redirect("home");
		}
		$data = array(
			"title"			=> ucwords($pc1)." ".strtoupper($gVar['pageName']),
			"slugLoaded"	=> $slugLoaded,
			"pageName"		=> $gVar['pageName'],
			"akses"			=> $gVar['akses'],
			"link_list"		=> $gVar['linkList'],
			"kolomID"		=> $gVar['kolomID'],
		);
		switch($pc1):
			case 'daftar':
				$where = array(
					"{$gVar['akses']}_username" => $this->session->userdata("{$gVar['akses']}_username"),
				);
				$inMatkul = array();
				$data['data_matkul']	= $this->model_data->fetch_default("matakuliah", $where);
				if($data['data_matkul']):
					foreach($data['data_matkul'] as $key=>$val):
						$inMatkul[] = $val->id_matkul;
					endforeach;

					$data['data_column'] = $this->model_data->fetch_column_name($gVar['pageName']);
					$data['data_tugas'] = $this->model_data->fetch_default($gVar['pageName'],$inMatkul, "=", "*", "", "", "id_matkul");
					$data['data_tugas_kumpul'] = $this->model_data->fetch_default("{$gVar['pageName']}_kumpul");
					foreach($data['data_tugas'] as $key=>$val):
						//init jumlah
						$data['data_tugas'][$key]->jumlah_file = 0;
						foreach($data['data_tugas_kumpul'] as $key1=>$val1):
							if($data['data_tugas_kumpul'][$key1]->id_tugas == $data['data_tugas'][$key]->id_tugas)
								$data['data_tugas'][$key]->jumlah_file++;			
						endforeach;	
					endforeach;
				endif;
			break;
			case 'daftarfile':
				if(!$this->input->get("{$gVar['kolomID']}"))
					redirect(base_url("{$gVar['akses']}/{$gVar['pageName']}/daftar"));
				$query = $this->input->get("{$gVar['kolomID']}");
				$where = array(
					"{$gVar['kolomID']}" => $query,
				);
				$data['data_column'] = $this->model_data->fetch_column_name("tugas_kumpul");
				$data["data_tugas_kumpul"] = $this->model_data->fetch_default("tugas_kumpul",$where);
			break;
			case 'tambah':
			case 'edit':
				$where = array(
					"{$gVar['akses']}_username" => $this->session->userdata("{$gVar['akses']}_username"),
				);
				$data['FFile']			= $gVar['FFile'];
				$data['FNama']			= $gVar['FNama'];
				$data['data_matkul']	= $this->model_data->fetch_default("matakuliah", $where);
				$data['data_column'] = $this->model_data->fetch_column_name("{$gVar['pageName']}");

				if(isset($data['data_matkul']) AND $data['data_matkul']!=null):
					switch($pc1):
						case 'edit':
							if(!$this->input->get("{$gVar['kolomID']}"))
								redirect(base_url("{$gVar['akses']}/{$gVar['pageName']}/daftar"));
							$query = $this->input->get("{$gVar['kolomID']}");
							$where = array(
								"{$gVar['kolomID']}" => $query,
							);
							$data["data_{$gVar['pageName']}"] = $this->model_data->fetch_default("{$gVar['pageName']}",$where);
							if($data["data_{$gVar['pageName']}"]==null)
								redirect(base_url("{$gVar['akses']}/{$gVar['pageName']}/daftar"));
						break;
					endswitch;
				else:
					alert_danger("Data kosong", "Harap isi mata kuliah terlebih dahulu");	
					redirect(base_url("{$gVar['akses']}/matakuliah/tambah"));
				endif;

			break;
			case 'hapus':
				if(!$this->input->get("{$gVar['kolomID']}"))
					redirect(base_url("{$gVar['akses']}/{$gVar['pageName']}/daftar"));
				$query = $this->input->get("{$gVar['kolomID']}");
				$where = array(
					"{$gVar['kolomID']}" => $query,
				);
				$act = $this->model_data->delete_default("{$gVar['pageName']}", $where);
				if($act){
					alert_success("Sukses Dihapus", "{$gVar['pageName']} {$query} telah dihapus");
				}else{
					alert_success("Gagal Dihapus", "{$gVar['pageName']} {$query} gagal dihapus");					
				}
				redirect(base_url("{$gVar['akses']}/{$gVar['pageName']}/daftar"));
			break;

			/*
			===========
			AKSI
			===========
			*/
			case 'tambah_act':
				$params		= $this->input->post(null, true);
				$FFile =  implode("|",$params['inputFFile']);
				$FNama =  implode("{$params['inputFNamaDelimiter']}",$params['inputFNama']);
				if(!isset($params)){
					redirect(base_url("{$gVar['akses']}/{$gVar['pageName']}/tambah"));
				}
				$cek = array("nama_{$gVar['pageName']}" 	=> $params["inputNamaTugas"]);
				if($this->model_data->fetch_default("{$gVar['pageName']}", $cek)){
					alert_danger("Gagal Tambah Tugas", "Telah ada nama yang serupa");
					redirect(base_url("{$gVar['akses']}/{$gVar['pageName']}/daftar"));
				}
				$data_insert = array(
					"id_matkul" 				=> "{$params['inputMatkul']}",
					"nama_{$gVar['pageName']}" 	=> "{$params['inputNamaTugas']}",
					"format_file"				=> "{$FFile}",
					"format_nama_delimiter"		=> "{$params['inputFNamaDelimiter']}",
					"format_nama"				=> "{$FNama}",
					"deskripsi"					=> "{$params['inputDeskripsi']}",
					"waktu_akhir"				=> "{$params['inputWaktuAkhir']}",
				);
				$act = $this->model_data->insert_default("{$gVar['pageName']}", $data_insert);
				if($act){
					alert_success("Sukses tambah {$gVar['pageName']}", "{$gVar['pageName']} sukses tambah");
				}else{
					alert_danger("Gagal tambah {$gVar['pageName']}", "Terjadi kesalahan, silahkan ulangi lagi");
				}
				redirect(base_url("{$gVar['akses']}/{$gVar['pageName']}/daftar"));
			break;
			case 'edit_act':
				$params = $this->input->post(null, true);
				$FFile =  implode("|",$params['inputFFile']);
				$FNama =  implode("{$params['inputFNamaDelimiter']}",$params['inputFNama']);
				$where = array(
					"{$gVar['kolomID']}" => $params["{$gVar['kolomID']}"],
				);
				if(!isset($params) || !isset($params["{$gVar['kolomID']}"]))
					redirect(base_url("{$gVar['akses']}/{$gVar['pageName']}/daftar"));

				$cek = array(
					"nama_{$gVar['pageName']}" 	=> $params["inputNamaTugas"],
					"{$gVar['kolomID']}" 		=> "!={$params['inputNamaTugas']}",
				);
				if($this->model_data->fetch_default("{$gVar['pageName']}", $cek)){
					alert_danger("Gagal Ubah Tugas", "Telah ada nama yang serupa");
					redirect(base_url("{$gVar['akses']}/{$gVar['pageName']}/daftar"));
				}
				$data_insert = array(
					"nama_{$gVar['pageName']}" 	=> "{$params['inputNamaTugas']}",
					"format_file"				=> "{$FFile}",
					"format_nama_delimiter"		=> "{$params['inputFNamaDelimiter']}",
					"format_nama"				=> "{$FNama}",
					"deskripsi"					=> "{$params['inputDeskripsi']}",
					"waktu_akhir"				=> "{$params['inputWaktuAkhir']}",
				);
				$act = $this->model_data->update_default("{$gVar['pageName']}", $data_insert, $where);
				if($act){
					alert_success("Sukses diubah", "{$gVar['pageName']} dengan id {$params["{$gVar['kolomID']}"]} sukses diubah");
				}else{
					alert_danger("Gagal ubah", "Terjadi kesalahan, silahkan ulangi lagi");
				}
				redirect(base_url("{$gVar['akses']}/{$gVar['pageName']}/daftar"));
			break;
			case 'daftarfile_act':
				if(!$this->input->get("act"))
					redirect(base_url("{$gVar['akses']}/{$gVar['pageName']}/daftarfile"));
				$actJenis = $this->input->get("act");
				if($actJenis=="hapus"){
					$query = $this->input->get("id_kumpul");
					$where = array("id_kumpul" => $query,);
					$data['data_tugas'] = $this->model_data->fetch_default("tugas_kumpul", $where);
					$id_tugas = $data['data_tugas'][0]->id_tugas;
					$fileChecked = "./uploads/tugas/{$id_tugas}/{$data['data_tugas'][0]->nama_file}";
					//Jika file ada, langsung delete
					if(is_file($fileChecked)){
						unlink($fileChecked); 
					}
					$act = $this->model_data->delete_default("{$gVar['pageName']}_kumpul", $where);
				}else if($actJenis=="nilai"){
					$id_tugas = $this->input->post("id_tugas");
					$nilaiArray = $this->input->post("batch_nilai");
					$data_update = array();
					foreach($nilaiArray as $key=>$val){
						$data_update[] = array(
							"id_kumpul" => $key,
							"nilai" => $val,
							"waktu_nilai" => date("Y-m-d H:i:s"),
						);
					}
					$query = "lebih dari 1";
					$act = $this->model_data->update_default("{$gVar['pageName']}_kumpul", $data_update, "id_kumpul", "batch");
				}

				if($act){
					alert_success("Sukses {$actJenis}", "{$gVar['pageName']} dengan id $query sukses di{$actJenis}");
				}else{
					alert_danger("Gagal {$actJenis}", "Terjadi kesalahan, silahkan ulangi lagi");
				}

				redirect(base_url("{$gVar['akses']}/{$gVar['pageName']}/daftarfile?{$gVar['kolomID']}={$id_tugas}"));
			break;
		endswitch;

		$this->load->view("{$gVar['akses']}/{$gVar['pageName']}", $data);
	}
}