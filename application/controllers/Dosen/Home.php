<?php
defined("BASEPATH") OR exit("No direct script access allowed");
class Home extends CI_Controller {

	public function __construct(){
		parent::__construct();

		//gVar = Global Variabel
		$akses = strtolower($this->uri->segment(1));
		$pageName = $this->router->fetch_class();

		$this->gVar = array(
			'akses' 	=> $akses,
			'pageName'	=> $this->router->fetch_class(),
			'linkList' 	=> $this->model_link_list->link_list($akses),
			// 'kolomID' 	=> $this->model_data->fetch_column_PK($pageName),
		);
		$this->model_security->logged_in($akses);
	}

	public function index(){
		$gVar = $this->gVar;
		$slugLoaded = $gVar['pageName'];
		
		if ( ! file_exists(APPPATH."views/{$gVar['akses']}/{$gVar['pageName']}.php")){
			redirect("home");
		}
		$where = array(
			"{$gVar['akses']}_username" => $this->session->userdata("{$gVar['akses']}_username"),
		);
		$data = array(
			"title"			=> ucwords($gVar['pageName'])." ".strtoupper($gVar['akses']),
			"pageName"		=> $gVar['pageName'],
			"slugLoaded"	=> $slugLoaded,
			"akses"			=> $gVar['akses'],
			"link_list"		=> $gVar['linkList'],
			"data_matkul" => $this->model_data->fetch_default("matakuliah", $where),
		);

		$inMatkul = array();
		$inTugas = array();
		$data['data_tugas'] = array();
		$data['data_tugas_kumpul'] = array();
		if($data['data_matkul']):
			foreach($data['data_matkul'] as $key=>$val):
				$inMatkul[] = $val->id_matkul;
			endforeach;
			$data['data_tugas'] = $this->model_data->fetch_default("tugas",$inMatkul, "=", "*", "", "", "id_matkul");
			if($data['data_tugas']):
				foreach($data['data_tugas'] as $key=>$val):
					$inTugas[] = $val->id_tugas;
				endforeach;
				$data['data_tugas_kumpul'] = $this->model_data->fetch_default("tugas_kumpul",$inTugas, "=", "*", "", "", "id_tugas");
			endif;
		endif;

		$this->load->view("{$gVar['akses']}/{$gVar['pageName']}", $data);
	}

}