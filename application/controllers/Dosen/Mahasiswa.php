<?php
defined("BASEPATH") OR exit("No direct script access allowed");
class Mahasiswa extends CI_Controller {

	public function __construct(){
		parent::__construct();


		//gVar = Global Variabel
		$akses = strtolower($this->uri->segment(1));
		$pageName = $this->router->fetch_class();

		$this->gVar = array(
			'akses' 	=> $akses,
			'pageName'	=> $this->router->fetch_class(),
			'linkList' 	=> $this->model_link_list->link_list($akses),
			'kolomID' 	=> $this->model_data->fetch_column_PK("{$pageName}_login"),
		);
		$this->model_security->logged_in($akses);
	}

	public function index($pc1 = "daftar", $pc2 = ""){
		$gVar = $this->gVar;
		$slugLoaded = $gVar['pageName'];
		$slugLoaded .= ($pc1!="") ? "/{$pc1}" : "" ;
		
		if ( ! file_exists(APPPATH."views/{$gVar['akses']}/{$gVar['pageName']}.php")){
			redirect("home");
		}

		$data = array(
			"title"			=> ucwords($pc1)." ".strtoupper($gVar['pageName']),
			"pageName"		=> $gVar['pageName'],
			"slugLoaded"	=> $slugLoaded,
			"akses"			=> $gVar['akses'],
			"link_list"		=> $gVar['linkList'],
			"kolomID"		=> $gVar['kolomID'],
		);

		switch($pc1):
			case 'daftar':
				$data["data_column"] = $this->model_data->fetch_column_name($gVar['pageName']."_login");
				$data["data_{$gVar['pageName']}"] = $this->model_data->fetch_default($gVar['pageName']."_login");
			break;
			/*
			===========
			AKSI
			===========
			*/
		endswitch;
		$this->load->view("{$gVar['akses']}/{$gVar['pageName']}", $data);
	}
}