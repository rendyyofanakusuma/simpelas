<?php
defined("BASEPATH") OR exit("No direct script access allowed");
class Konten extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index(){
		$pageName = "konten";
		if ( ! file_exists(APPPATH."views/{$pageName}.php")){
			// Whoops, we don"t have a page for that!
			show_404();
		}
		$data["title"] = ucwords($pageName); // Capitalize the first letter
		$this->load->view($pageName, $data);
	}

}