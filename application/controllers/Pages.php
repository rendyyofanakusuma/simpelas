<?php
class Pages extends CI_Controller {

	public function view($pageName = "home"){
		if ( ! file_exists(APPPATH."views/{$pageName}.php")){
			// Whoops, we don"t have a page for that!
			show_404();
		}

		$data["title"] = ucwords($pageName); // Capitalize the first letter

		$this->load->view("inc/header", $data);
		$this->load->view("pages/".$pageName, $data);
		$this->load->view("inc/footer", $data);
	}
}
?>