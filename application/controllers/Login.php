<?php
defined("BASEPATH") OR exit("No direct script access allowed");
class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index($akses="mahasiswa"){
		$pageName = "login";
		$slugLoaded = $pageName;
		$akses = strtolower($akses);

		if($this->session->userdata("{$akses}_logged_in") OR $this->input->cookie("{$akses}_remember_token")){
			redirect("{$akses}/home/");
		}else{
			$this->model_security->batch_delete_cookies($akses);
			//Jika halaman tidak ada
			if (!file_exists(APPPATH."controllers/{$akses}"))
				redirect("home");
			$data = array(
				"title"		=> ucwords($pageName)." ".strtoupper($akses),
				"pageName"		=> $pageName,
				"slugLoaded"	=> $slugLoaded,
				"akses"		=> $akses,
			);
			switch($akses){
				case "mahasiswa" :
					$data['btn_warna'] = "success";
					$data['login_type_id'] = "NIM";
					$data['username_field_length_max'] = 12;
					$data['username_field_length_min'] = $data['username_field_length_max'];
					break;
				case "dosen":
					$data['btn_warna'] = "dark";
					$data['login_type_id'] = "NIP / NITP";
					$data['username_field_length_max'] = 18;
					$data['username_field_length_min'] = 9;
					break;
				case "admin":
					$data['btn_warna'] = "danger";
					$data['username_field_length_max'] = 20;
					$data['username_field_length_min'] = 1;
					break;
			}
			$this->load->view($pageName, $data);			
		}
	}

	public function login_act($akses="mahasiswa"){
		$pageName = "login";
		$params=$this->input->post(null, true);
		$username = $params['username'];
		$password = md5($params['password']);
		if($this->model_akun->do_login($username, $password, $akses)){
			if($this->input->post('remember')) {
				$cookieName = array("{$akses}_remember_token","{$akses}_dt1","{$akses}_dt2");
				$cookieVal = array(TRUE,"{$username}","{$password}");
				for($i = 0;$i<count($cookieName);$i++)
					$this->model_security->batch_cookies_set($cookieName[$i],$cookieVal[$i]);
			}
			$this->session->set_userdata("{$akses}_logged_in",TRUE);
			$this->session->set_userdata("{$akses}_username",$username);
			$this->session->set_userdata("{$akses}_password",$password);
			alert_success("Selamat Datang", "Anda login sebagai {$akses} dengan nama ".$this->session->userdata("{$akses}_username"));
			redirect("{$akses}/home");
		}else{
			alert_danger("Gagal Login", "Username dan/atau Password yang anda masukkan tidak sesuai");
			redirect($pageName."/{$akses}");
		}
	}
}